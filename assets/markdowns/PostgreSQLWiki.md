<!--[if lt IE 9]><script src="/w/resources/lib/html5shiv/html5shiv.js"></script><![endif]-->

[](<>)

[![Banner logo](PostgreSQL%20-%20Wikipedia_files/70px-WAM_logo_without_text.png)](https://en.wikipedia.org/wiki/Wikipedia:Wikipedia_Asian_Month/2019)

[This November is Wikipedia Asian month.  
Join the contest and win a postcard from Asia.](https://en.wikipedia.org/wiki/Wikipedia:Wikipedia_Asian_Month/2019)

\[[Help with translations!](https://meta.wikimedia.org/w/index.php?title=Special:Translate&group=Centralnotice-tgroup-asian_month_2019)]

[![Hide](PostgreSQL%20-%20Wikipedia_files/CloseWindow19x19.png)](# "Hide")

<!-- CentralNotice -->

# PostgreSQL

From Wikipedia, the free encyclopedia

[Jump to navigation](#mw-head) [Jump to search](#p-search)

Free and open-source relational database management system



| [![The World's Most Advanced Open Source Relational Database\[1\]](PostgreSQL%20-%20Wikipedia_files/220px-Postgresql_elephant.png)](https://en.wikipedia.org/wiki/File:Postgresql_elephant.svg "The World's Most Advanced Open Source Relational Database\[1]")_The World's Most Advanced Open Source Relational Database_[\[1\]](#cite_note-1) |                                                                                                                                                                                                                                                                                                                                         |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Developer(s)](https://en.wikipedia.org/wiki/Software_developer "Software developer")                                                                                                                                                                                                                                                           | PostgreSQL Global Development Group                                                                                                                                                                                                                                                                                                     |
| Initial release                                                                                                                                                                                                                                                                                                                                 | 8 July 1996;  
23 years ago(1996-07-08)[\[2\]](#cite_note-birthday-2)                                                                                                                                                                                                                                                                   |
|                                                                                                                                                                                                                                                                                                                                                 |                                                                                                                                                                                                                                                                                                                                         |
| [Stable release](https://en.wikipedia.org/wiki/Software_release_life_cycle "Software release life cycle")                                                                                                                                                                                                                                       | 12.0 / 3 October 2019;  
40 days ago(2019-10-03)[\[3\]](#cite_note-release-3)                                                                                                                                                                                                                                                           |
|                                                                                                                                                                                                                                                                                                                                                 |                                                                                                                                                                                                                                                                                                                                         |
| [Repository](https://en.wikipedia.org/wiki/Repository_(version_control) "Repository (version control)")                                                                                                                                                                                                                                         | -   [git​.postgresql​.org​/gitweb​/?p=postgresql​.git](https://git.postgresql.org/gitweb/?p=postgresql.git)[![Edit this at Wikidata](PostgreSQL%20-%20Wikipedia_files/10px-OOjs_UI_icon_edit-ltr-progressive.png)](https://www.wikidata.org/wiki/Q192490#P1324 "Edit this at Wikidata")                                                 |
| Written in                                                                                                                                                                                                                                                                                                                                      | [C](https://en.wikipedia.org/wiki/C_(programming_language) "C (programming language)")                                                                                                                                                                                                                                                  |
| [Operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system")                                                                                                                                                                                                                                                           | [FreeBSD](https://en.wikipedia.org/wiki/FreeBSD "FreeBSD"), [Linux](https://en.wikipedia.org/wiki/Linux "Linux"), [macOS](https://en.wikipedia.org/wiki/MacOS "MacOS"), [OpenBSD](https://en.wikipedia.org/wiki/OpenBSD "OpenBSD"), [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows "Microsoft Windows")[\[4\]](#cite_note-4) |
| [Type](https://en.wikipedia.org/wiki/Software_categories#Categorization_approaches "Software categories")                                                                                                                                                                                                                                       | [RDBMS](https://en.wikipedia.org/wiki/Relational_database "Relational database")                                                                                                                                                                                                                                                        |
| [License](https://en.wikipedia.org/wiki/Software_license "Software license")                                                                                                                                                                                                                                                                    | PostgreSQL License ([free and open-source](https://en.wikipedia.org/wiki/Free_and_open-source "Free and open-source"), [permissive](https://en.wikipedia.org/wiki/Permissive_software_licence "Permissive software licence"))[\[5\]](#cite_note-about/licence-5)[\[6\]](#cite_note-approved_by_OSI-6)[\[7\]](#cite_note-OSI-7)          |
| Website                                                                                                                                                                                                                                                                                                                                         | [postgresql​.org](http://postgresql.org/)                                                                                                                                                                                                                                                                                               |

| [Publisher](https://en.wikipedia.org/wiki/Publishing "Publishing")                                                                                                                                                                                       | PostgreSQL Global Development Group  
Regents of the University of California |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| [DFSG](https://en.wikipedia.org/wiki/Debian_Free_Software_Guidelines "Debian Free Software Guidelines") compatible                                                                                                                                       | Yes[\[8\]](#cite_note-8)[\[9\]](#cite_note-9)                                 |
| [FSF](https://en.wikipedia.org/wiki/Free_Software_Foundation "Free Software Foundation") [approved](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses#Approvals "Comparison of free and open-source software licenses") | Yes[\[10\]](#cite_note-10)                                                    |
| [OSI](https://en.wikipedia.org/wiki/Open_Source_Initiative "Open Source Initiative") [approved](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses#Approvals "Comparison of free and open-source software licenses")     | Yes[\[7\]](#cite_note-OSI-7)                                                  |
| [GPL compatible](https://en.wikipedia.org/wiki/GPL_compatible "GPL compatible")                                                                                                                                                                          | Yes                                                                           |
| [Copyleft](https://en.wikipedia.org/wiki/Copyleft "Copyleft")                                                                                                                                                                                            | No                                                                            |
| [Linking from code with a different license](https://en.wikipedia.org/wiki/GPL_linking_exception "GPL linking exception")                                                                                                                                | Yes                                                                           |
| Website                                                                                                                                                                                                                                                  | [postgresql​.org​/about​/licence](http://postgresql.org/about/licence)        |

**PostgreSQL**, also known as **Postgres**, is a [free and open-source](https://en.wikipedia.org/wiki/Free_and_open-source_software "Free and open-source software") [relational database](https://en.wikipedia.org/wiki/Relational_database "Relational database") management system (RDBMS) emphasizing [extensibility](https://en.wikipedia.org/wiki/Extensibility "Extensibility") and [technical standards](https://en.wikipedia.org/wiki/Technical_standard "Technical standard") compliance. It is designed to handle a range of workloads, from single machines to [data warehouses](https://en.wikipedia.org/wiki/Data_warehouse "Data warehouse") or [Web services](https://en.wikipedia.org/wiki/Web_services "Web services") with many [concurrent users](https://en.wikipedia.org/wiki/Concurrent_user "Concurrent user"). It is the default database for [macOS Server](https://en.wikipedia.org/wiki/MacOS_Server "MacOS Server"),[\[11\]](#cite_note-OS_X_Lion_Server-11)[\[12\]](#cite_note-MySQL_not_included-12)[\[13\]](#cite_note-OS_X-13) and is also available for [Linux](https://en.wikipedia.org/wiki/Linux "Linux"), [FreeBSD](https://en.wikipedia.org/wiki/FreeBSD "FreeBSD"), [OpenBSD](https://en.wikipedia.org/wiki/OpenBSD "OpenBSD"), and [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows "Microsoft Windows").

PostgreSQL features [transactions](https://en.wikipedia.org/wiki/Transaction_processing "Transaction processing") with [Atomicity, Consistency, Isolation, Durability](https://en.wikipedia.org/wiki/ACID_(computer_science) "ACID (computer science)") (ACID) properties, automatically updatable [views](https://en.wikipedia.org/wiki/View_(SQL) "View (SQL)"), [materialized views](https://en.wikipedia.org/wiki/Materialized_view "Materialized view"), [triggers](https://en.wikipedia.org/wiki/Database_trigger "Database trigger"), [foreign keys](https://en.wikipedia.org/wiki/Foreign_key "Foreign key"), and [stored procedures](https://en.wikipedia.org/wiki/Stored_procedure "Stored procedure").[\[14\]](#cite_note-intro-whatis-14) PostgreSQL is developed by the PostgreSQL Global Development Group, a diverse group of many companies and individual contributors.[\[15\]](#cite_note-contributors-15)

\[ ]

## Contents

-   [1 Name](#Name)

-   [2 History](#History)

-   [3 Multiversion concurrency control (MVCC)](#Multiversion_concurrency_control_(MVCC))

-   [4 Storage and replication](#Storage_and_replication)

    -   [4.1 Replication](#Replication)
    -   [4.2 Indexes](#Indexes)
    -   [4.3 Schemas](#Schemas)
    -   [4.4 Data types](#Data_types)
    -   [4.5 User-defined objects](#User-defined_objects)
    -   [4.6 Inheritance](#Inheritance)
    -   [4.7 Other storage features](#Other_storage_features)

-   [5 Control and connectivity](#Control_and_connectivity)

    -   [5.1 Foreign data wrappers](#Foreign_data_wrappers)
    -   [5.2 Interfaces](#Interfaces)
    -   [5.3 Procedural languages](#Procedural_languages)
    -   [5.4 Triggers](#Triggers)
    -   [5.5 Asynchronous notifications](#Asynchronous_notifications)
    -   [5.6 Rules](#Rules)
    -   [5.7 Other querying features](#Other_querying_features)
    -   [5.8 Concurrency model](#Concurrency_model)

-   [6 Security](#Security)

-   [7 Standards compliance](#Standards_compliance)

-   [8 Benchmarks and performance](#Benchmarks_and_performance)

-   [9 Platforms](#Platforms)

-   [10 Database administration](#Database_administration)

-   [11 Notable users](#Notable_users)

-   [12 Service implementations](#Service_implementations)

-   [13 Release history](#Release_history)

-   [14 See also](#See_also)

-   [15 References](#References)

-   [16 Further reading](#Further_reading)

-   [17 External links](#External_links)

## Name\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=1 "Edit section: Name")]

PostgreSQL's developers pronounce PostgreSQL as [/ˈpoʊstɡrɛsˌkjuːˈɛl/](https://en.wikipedia.org/wiki/Help:IPA/English "Help:IPA/English").[\[16\]](#cite_note-Audio_sample-16) It is abbreviated as _Postgres_ because of ubiquitous support for the [SQL](https://en.wikipedia.org/wiki/SQL "SQL") standard among relational databases. Originally named POSTGRES, the name (Post [Ingres](https://en.wikipedia.org/wiki/Ingres_(database) "Ingres (database)")) refers to the project's origins in that RDBMS that originated at [University of California, Berkeley](https://en.wikipedia.org/wiki/University_of_California,_Berkeley "University of California, Berkeley").[\[17\]](#cite_note-design-17)[\[18\]](#cite_note-about/history-18) After a review the PostgreSQL Core Team announced in 2007 that the product would continue to use the name PostgreSQL.[\[19\]](#cite_note-Project_name-19)

## History\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=2 "Edit section: History")]

PostgreSQL evolved from the [Ingres](https://en.wikipedia.org/wiki/Ingres_(database) "Ingres (database)") project at the University of California, Berkeley. In 1982, the leader of the Ingres team, [Michael Stonebraker](https://en.wikipedia.org/wiki/Michael_Stonebraker "Michael Stonebraker"), left Berkeley to make a proprietary version of Ingres.[\[17\]](#cite_note-design-17) He returned to Berkeley in 1985, and began a post-Ingres project to address the problems with contemporary database systems that had become increasingly clear during the early 1980s. He won the [Turing Award](https://en.wikipedia.org/wiki/Turing_Award "Turing Award") in 2014 for these and other projects,[\[20\]](#cite_note-20) and techniques pioneered in them.

The new project, POSTGRES, aimed to add the fewest features needed to completely support [data types](https://en.wikipedia.org/wiki/Data_type "Data type").[\[21\]](#cite_note-Stonebraker-21) These features included the ability to define types and to fully describe relationships – something used widely, but maintained entirely by the user. In POSTGRES, the database understood relationships, and could retrieve information in related tables in a natural way using _rules_. POSTGRES used many of the ideas of Ingres, but not its code.[\[22\]](#cite_note-pavel-history-22)

Starting in 1986, published papers described the basis of the system, and a prototype version was shown at the 1988 ACM [SIGMOD](https://en.wikipedia.org/wiki/SIGMOD "SIGMOD") Conference. The team released version 1 to a small number of users in June 1989, followed by version 2 with a re-written rules system in June 1990. Version 3, released in 1991, again re-wrote the rules system, and added support for multiple storage managers\[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] and an improved query engine. By 1993, the number of users began to overwhelm the project with requests for support and features. After releasing version 4.2[\[23\]](#cite_note-University_POSTGRES-23) on June 30, 1994 – primarily a cleanup – the project ended. Berkeley released POSTGRES under an [MIT License](https://en.wikipedia.org/wiki/MIT_License "MIT License") variant, which enabled other developers to use the code for any use. At the time, POSTGRES used an Ingres-influenced [POSTQUEL query language](https://en.wikipedia.org/wiki/QUEL_query_languages "QUEL query languages") interpreter, which could be interactively used with a [console application](https://en.wikipedia.org/wiki/Console_application "Console application") named `monitor`.

In 1994, Berkeley graduate students Andrew Yu and Jolly Chen replaced the POSTQUEL query language interpreter with one for the SQL query language, creating Postgres95. `monitor` was also replaced by `psql`. Yu and Chen announced the first version (0.01) to [beta testers](https://en.wikipedia.org/wiki/Beta_tester "Beta tester") on May 5, 1995. Version 1.0 of Postgres95 was announced on September 5, 1995, with a more liberal license that enabled the software to be freely modifiable.

On July 8, 1996, Marc Fournier at Hub.org Networking Services provided the first non-university development server for the open-source development effort.[\[2\]](#cite_note-birthday-2) With the participation of Bruce Momjian and Vadim B. Mikheev, work began to stabilize the code inherited from Berkeley.

In 1996, the project was renamed to PostgreSQL to reflect its support for SQL. The online presence at the website PostgreSQL.org began on October 22, 1996.[\[24\]](#cite_note-20th_anniversary-24) The first PostgreSQL release formed version 6.0 on January 29, 1997. Since then developers and volunteers around the world have maintained the software as The PostgreSQL Global Development Group.[\[15\]](#cite_note-contributors-15)

The project continues to make releases available under its [free and open-source software](https://en.wikipedia.org/wiki/Free_and_open-source_software "Free and open-source software") PostgreSQL License. Code comes from contributions from proprietary vendors, support companies, and open-source programmers.

## Multiversion concurrency control (MVCC)\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=3 "Edit section: Multiversion concurrency control (MVCC)")]

PostgreSQL manages [concurrency](https://en.wikipedia.org/wiki/Concurrency_control "Concurrency control") through [multiversion concurrency control](https://en.wikipedia.org/wiki/Multiversion_concurrency_control "Multiversion concurrency control") (MVCC), which gives each transaction a "snapshot" of the database, allowing changes to be made without affecting other transactions. This largely eliminates the need for read locks, and ensures the database maintains [ACID](https://en.wikipedia.org/wiki/ACID_(computer_science) "ACID (computer science)") principles. PostgreSQL offers three levels of [transaction isolation](https://en.wikipedia.org/wiki/Isolation_(database_systems) "Isolation (database systems)"): Read Committed, Repeatable Read and Serializable. Because PostgreSQL is immune to dirty reads, requesting a Read Uncommitted transaction isolation level provides read committed instead. PostgreSQL supports full [serializability](https://en.wikipedia.org/wiki/Serializability "Serializability") via the serializable [snapshot isolation](https://en.wikipedia.org/wiki/Snapshot_isolation "Snapshot isolation") (SSI) method.[\[25\]](#cite_note-ports-25)

## Storage and replication\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=4 "Edit section: Storage and replication")]

### Replication\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=5 "Edit section: Replication")]

PostgreSQL includes built-in binary replication based on shipping the changes ([write-ahead logs](https://en.wikipedia.org/wiki/Write-ahead_logging "Write-ahead logging") (WAL)) to replica nodes asynchronously, with the ability to run read-only queries against these replicated nodes. This allows splitting read traffic among multiple nodes efficiently. Earlier replication software that allowed similar read scaling normally relied on adding replication triggers to the master, increasing load.

PostgreSQL includes built-in synchronous replication[\[26\]](#cite_note-H_Online-26) that ensures that, for each write transaction, the master waits until at least one replica node has written the data to its transaction log. Unlike other database systems, the durability of a transaction (whether it is asynchronous or synchronous) can be specified per-database, per-user, per-session or even per-transaction. This can be useful for workloads that do not require such guarantees, and may not be wanted for all data as it slows down performance due to the requirement of the confirmation of the transaction reaching the synchronous standby.

Standby servers can be synchronous or asynchronous. Synchronous standby servers can be specified in the configuration which determines which servers are candidates for synchronous replication. The first in the list that is actively streaming will be used as the current synchronous server. When this fails, the system fails over to the next in line.

Synchronous [multi-master replication](https://en.wikipedia.org/wiki/Multi-master_replication "Multi-master replication") is not included in the PostgreSQL core. Postgres-XC which is based on PostgreSQL provides scalable synchronous multi-master replication.[\[27\]](#cite_note-Postgres-XC-27) It is licensed under the same license as PostgreSQL. A related project is called [Postgres-XL](https://en.wikipedia.org/wiki/Postgres-XL "Postgres-XL"). Postgres-R is yet another [fork](https://en.wikipedia.org/wiki/Fork_(software_development) "Fork (software development)").[\[28\]](#cite_note-postgres-r-28) Bidirectional replication (BDR) is an asynchronous multi-master replication system for PostgreSQL.[\[29\]](#cite_note-bdr-29)

Tools such as repmgr make managing replication clusters easier.

Several asynchronous trigger-based replication packages are available. These remain useful even after introduction of the expanded core abilities, for situations where binary replication of a full database cluster is inappropriate:

-   [Slony-I](https://en.wikipedia.org/wiki/Slony-I "Slony-I")
-   Londiste, part of SkyTools (developed by [Skype](https://en.wikipedia.org/wiki/Skype "Skype"))
-   Bucardo multi-master replication (developed by [Backcountry.com](https://en.wikipedia.org/wiki/Backcountry.com "Backcountry.com"))[\[30\]](#cite_note-Fischer-30)
-   [SymmetricDS](https://en.wikipedia.org/wiki/SymmetricDS "SymmetricDS") multi-master, multi-tier replication

### Indexes\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=6 "Edit section: Indexes")]

PostgreSQL includes built-in support for regular [B-tree](https://en.wikipedia.org/wiki/B-tree "B-tree") and [hash table](https://en.wikipedia.org/wiki/Hash_table "Hash table") indexes, and four index access methods: generalized search trees ([GiST](https://en.wikipedia.org/wiki/GiST "GiST")), generalized [inverted indexes](https://en.wikipedia.org/wiki/Inverted_index "Inverted index") (GIN), Space-Partitioned GiST (SP-GiST)[\[31\]](#cite_note-SP-GiST-31) and [Block Range Indexes](https://en.wikipedia.org/wiki/Block_Range_Index "Block Range Index") (BRIN). In addition, user-defined index methods can be created, although this is quite an involved process. Indexes in PostgreSQL also support the following features:

-   [Expression indexes](https://en.wikipedia.org/wiki/Expression_index "Expression index") can be created with an index of the result of an expression or function, instead of simply the value of a column.
-   [Partial indexes](https://en.wikipedia.org/wiki/Partial_index "Partial index"), which only index part of a table, can be created by adding a WHERE clause to the end of the CREATE INDEX statement. This allows a smaller index to be created.
-   The planner is able to use multiple indexes together to satisfy complex queries, using temporary in-memory [bitmap index](https://en.wikipedia.org/wiki/Bitmap_index "Bitmap index") operations (useful for [data warehouse](https://en.wikipedia.org/wiki/Data_warehouse "Data warehouse") applications for joining a large [fact table](https://en.wikipedia.org/wiki/Fact_table "Fact table") to smaller [dimension tables](https://en.wikipedia.org/wiki/Dimension_table "Dimension table") such as those arranged in a [star schema](https://en.wikipedia.org/wiki/Star_schema "Star schema")).
-   [_k_-nearest neighbors](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm "K-nearest neighbors algorithm") (_k_-NN) indexing (also referred to KNN-GiST[\[32\]](#cite_note-KNN-GiST-32)) provides efficient searching of "closest values" to that specified, useful to finding similar words, or close objects or locations with [geospatial](https://en.wikipedia.org/wiki/Geographic_data_and_information "Geographic data and information") data. This is achieved without exhaustive matching of values.
-   Index-only scans often allow the system to fetch data from indexes without ever having to access the main table.
-   PostgreSQL 9.5 introduced [Block Range Indexes](https://en.wikipedia.org/wiki/Block_Range_Index "Block Range Index") (BRIN).

### Schemas\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=7 "Edit section: Schemas")]

In PostgreSQL, a [schema](https://en.wikipedia.org/wiki/Database_schema "Database schema") holds all objects, except for roles and tablespaces. Schemas effectively act like namespaces, allowing objects of the same name to co-exist in the same database. By default, newly created databases have a schema called _public_, but any further schemas can be added, and the public schema isn't mandatory.

A `search_path` setting determines the order in which PostgreSQL checks schemas for unqualified objects (those without a prefixed schema). By default, it is set to `$user, public` (`$user` refers to the currently connected database user). This default can be set on a database or role level, but as it is a session parameter, it can be freely changed (even multiple times) during a client session, affecting that session only.

Non-existent schemas listed in search_path are silently skipped during objects lookup.

New objects are created in whichever valid schema (one that presently exists) appears first in the search_path.Schema is an outline of database.

### Data types\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=8 "Edit section: Data types")]

A wide variety of native [data types](https://en.wikipedia.org/wiki/Data_type "Data type") are supported, including:

-   Boolean
-   [Arbitrary precision](https://en.wikipedia.org/wiki/Arbitrary-precision_arithmetic "Arbitrary-precision arithmetic") numerics
-   Character (text, varchar, char)
-   Binary
-   Date/time (timestamp/time with/without timezone, date, interval)
-   Money
-   Enum
-   Bit strings
-   Text search type
-   Composite
-   HStore, an extension enabled key-value store within PostgreSQL[\[33\]](#cite_note-33)
-   Arrays (variable length and can be of any data type, including text and composite types) up to 1 GB in total storage size
-   Geometric primitives
-   [IPv4](https://en.wikipedia.org/wiki/IPv4 "IPv4") and [IPv6](https://en.wikipedia.org/wiki/IPv6 "IPv6") addresses
-   [Classless Inter-Domain Routing](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing "Classless Inter-Domain Routing") (CIDR) blocks and [MAC addresses](https://en.wikipedia.org/wiki/MAC_address "MAC address")
-   [XML](https://en.wikipedia.org/wiki/XML "XML") supporting [XPath](https://en.wikipedia.org/wiki/XPath "XPath") queries
-   [Universally unique identifier](https://en.wikipedia.org/wiki/Universally_unique_identifier "Universally unique identifier") (UUID)
-   JavaScript Object Notation ([JSON](https://en.wikipedia.org/wiki/JSON "JSON")), and a faster [binary](https://en.wikipedia.org/wiki/Binary_code "Binary code") JSONB (since version 9.4; not the same as [BSON](https://en.wikipedia.org/wiki/BSON "BSON")[\[34\]](#cite_note-jsonb-34))

In addition, users can create their own data types which can usually be made fully indexable via PostgreSQL's indexing infrastructures – GiST, GIN, SP-GiST. Examples of these include the [geographic information system](https://en.wikipedia.org/wiki/Geographic_information_system "Geographic information system") (GIS) data types from the [PostGIS](https://en.wikipedia.org/wiki/PostGIS "PostGIS") project for PostgreSQL.

There is also a data type called a _domain_, which is the same as any other data type but with optional constraints defined by the creator of that domain. This means any data entered into a column using the domain will have to conform to whichever constraints were defined as part of the domain.

A data type that represents a range of data can be used which are called range types. These can be discrete ranges (e.g. all integer values 1 to 10) or continuous ranges (e.g., any time between 10:00 am and 11:00 am). The built-in range types available include ranges of integers, big integers, decimal numbers, time stamps (with and without time zone) and dates.

Custom range types can be created to make new types of ranges available, such as IP address ranges using the inet type as a base, or float ranges using the float data type as a base. Range types support inclusive and exclusive range boundaries using the `[`/`]` and `(`/`)` characters respectively. (e.g., `[4,9)` represents all integers starting from and including 4 up to but not including 9.) Range types are also compatible with existing operators used to check for overlap, containment, right of etc.

### User-defined objects\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=9 "Edit section: User-defined objects")]

New types of almost all objects inside the database can be created, including:

-   Casts
-   Conversions
-   Data types
-   [Data domains](https://en.wikipedia.org/wiki/Data_domain "Data domain")
-   Functions, including aggregate functions and window functions
-   Indexes including custom indexes for custom types
-   Operators (existing ones can be [overloaded](https://en.wikipedia.org/wiki/Operator_overloading "Operator overloading"))
-   Procedural languages

### Inheritance\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=10 "Edit section: Inheritance")]

Tables can be set to inherit their characteristics from a _parent_ table. Data in child tables will appear to exist in the parent tables, unless data is selected from the parent table using the ONLY keyword, i.e. `SELECT*FROMONLYparent_table;`. Adding a column in the parent table will cause that column to appear in the child table.

Inheritance can be used to implement table partitioning, using either triggers or rules to direct inserts to the parent table into the proper child tables.

As of 2010[\[update\]](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit), this feature is not fully supported yet – in particular, table constraints are not currently inheritable. All check constraints and not-null constraints on a parent table are automatically inherited by its children. Other types of constraints (unique, primary key, and foreign key constraints) are not inherited.

Inheritance provides a way to map the features of generalization hierarchies depicted in [entity relationship diagrams](https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model "Entity–relationship model") (ERDs) directly into the PostgreSQL database.

### Other storage features\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=11 "Edit section: Other storage features")]

-   [Referential integrity](https://en.wikipedia.org/wiki/Referential_integrity "Referential integrity") constraints including [foreign key](https://en.wikipedia.org/wiki/Foreign_key "Foreign key") constraints, column [constraints](https://en.wikipedia.org/wiki/Constraint_satisfaction "Constraint satisfaction"), and row checks
-   Binary and textual large-object storage
-   [Tablespaces](https://en.wikipedia.org/wiki/Tablespace "Tablespace")
-   Per-column collation
-   Online backup
-   Point-in-time recovery, implemented using write-ahead logging
-   In-place upgrades with pg_upgrade for less downtime (supports upgrades from 8.3.x and later)

## Control and connectivity\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=12 "Edit section: Control and connectivity")]

### Foreign data wrappers\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=13 "Edit section: Foreign data wrappers")]

PostgreSQL can link to other systems to retrieve data via foreign data wrappers (FDWs).[\[35\]](#cite_note-35) These can take the form of any data source, such as a file system, another [relational database](https://en.wikipedia.org/wiki/Relational_database "Relational database") management system (RDBMS), or a web service. This means that regular database queries can use these data sources like regular tables, and even join multiple data-sources together.

### Interfaces\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=14 "Edit section: Interfaces")]

PostgreSQL has several interfaces available and is also widely supported among programming language libraries. Built-in interfaces include libpq (PostgreSQL's official C application interface) and [ECPG](https://en.wikipedia.org/wiki/ECPG "ECPG") (an embedded C system). External interfaces include:

-   libpqxx: [C++](https://en.wikipedia.org/wiki/C%2B%2B "C++") interface
-   Pgfe: [C++](https://en.wikipedia.org/wiki/C%2B%2B "C++") interface
-   PostgresDAC: PostgresDAC (for Embarcadero RadStudio, Delphi, CBuilder XE-XE3)
-   DBD::Pg: [Perl](https://en.wikipedia.org/wiki/Perl "Perl") DBI driver
-   JDBC: [Java Database Connectivity](https://en.wikipedia.org/wiki/Java_Database_Connectivity "Java Database Connectivity") (JDBC) interface
-   Lua: Lua interface
-   Npgsql: [.NET](https://en.wikipedia.org/wiki/.NET_Framework ".NET Framework") data provider
-   ST-Links SpatialKit: Link Tool to [ArcGIS](https://en.wikipedia.org/wiki/ArcGIS "ArcGIS")
-   PostgreSQL.jl: [Julia](https://en.wikipedia.org/wiki/Julia_(programming_language) "Julia (programming language)") interface
-   node-postgres: [Node.js](https://en.wikipedia.org/wiki/Node.js "Node.js") interface
-   pgoledb: [OLE DB](https://en.wikipedia.org/wiki/OLE_DB "OLE DB") interface
-   psqlODBC: [Open Database Connectivity](https://en.wikipedia.org/wiki/Open_Database_Connectivity "Open Database Connectivity") (ODBC) interface
-   psycopg2:[\[36\]](#cite_note-psycopg2-36) Python interface (also used by [HTSQL](https://en.wikipedia.org/wiki/HTSQL "HTSQL"))
-   pgtclng: Tcl interface
-   pyODBC: Python library
-   php5-pgsql: PHP driver based on libpq
-   postmodern: A [Common Lisp](https://en.wikipedia.org/wiki/Common_Lisp "Common Lisp") interface
-   pq: A pure [Go](https://en.wikipedia.org/wiki/Go_(programming_language) "Go (programming language)") PostgreSQL driver for the Go database/sql package. The driver passes the compatibility test suite.[\[37\]](#cite_note-37)
-   RPostgreSQL: [R](https://en.wikipedia.org/wiki/R_(programming_language) "R (programming language)") interface[\[38\]](#cite_note-38)
-   dpq: [D](https://en.wikipedia.org/wiki/D_(programming_language) "D (programming language)") interface to libpq
-   epgsql: [Erlang](https://en.wikipedia.org/wiki/Erlang_(programming_language) "Erlang (programming language)") interface
-   Rust-Postgres: [Rust](https://en.wikipedia.org/wiki/Rust_(programming_language) "Rust (programming language)") interface
-   postgres: [Dart](https://en.wikipedia.org/wiki/Dart_(programming_language) "Dart (programming language)") interface

### Procedural languages\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=15 "Edit section: Procedural languages")]

Procedural languages allow developers to extend the database with custom [subroutines](https://en.wikipedia.org/wiki/Subroutine "Subroutine") (functions), often called _[stored procedures](https://en.wikipedia.org/wiki/Stored_procedure "Stored procedure")_. These functions can be used to build [database triggers](https://en.wikipedia.org/wiki/Database_trigger "Database trigger") (functions invoked on modification of certain data) and custom data types and [aggregate functions](https://en.wikipedia.org/wiki/Aggregate_function "Aggregate function").[\[39\]](#cite_note-39) Procedural languages can also be invoked without defining a function, using a DO command at SQL level.[\[40\]](#cite_note-40)

Languages are divided into two groups: Procedures written in _safe_ languages are [sandboxed](https://en.wikipedia.org/wiki/Sandbox_(computer_security) "Sandbox (computer security)") and can be safely created and used by any user. Procedures written in _unsafe_ languages can only be created by [superusers](https://en.wikipedia.org/wiki/Superuser "Superuser"), because they allow bypassing a database's security restrictions, but can also access sources external to the database. Some languages like Perl provide both safe and unsafe versions.

PostgreSQL has built-in support for three procedural languages:

-   Plain SQL (safe). Simpler SQL functions can get [expanded inline](https://en.wikipedia.org/wiki/Inline_expansion "Inline expansion") into the calling (SQL) query, which saves function call overhead and allows the query optimizer to "see inside" the function.
-   Procedural Language/PostgreSQL ([PL/pgSQL](https://en.wikipedia.org/wiki/PL/pgSQL "PL/pgSQL")) (safe), which resembles Oracle's Procedural Language for SQL ([PL/SQL](https://en.wikipedia.org/wiki/PL/SQL "PL/SQL")) procedural language and SQL/Persistent Stored Modules ([SQL/PSM](https://en.wikipedia.org/wiki/SQL/PSM "SQL/PSM")).
-   [C](https://en.wikipedia.org/wiki/C_(programming_language) "C (programming language)") (unsafe), which allows loading one or more custom [shared library](https://en.wikipedia.org/wiki/Shared_library "Shared library") into the database. Functions written in C offer the best performance, but bugs in code can crash and potentially corrupt the database. Most built-in functions are written in C.

In addition, PostgreSQL allows procedural languages to be loaded into the database through extensions. Three language extensions are included with PostgreSQL to support [Perl](https://en.wikipedia.org/wiki/Perl "Perl"), [Python](https://en.wikipedia.org/wiki/Python_(programming_language) "Python (programming language)")[\[41\]](#cite_note-41) and [Tcl](https://en.wikipedia.org/wiki/Tcl "Tcl"). There are external projects to add support for many other languages,[\[42\]](#cite_note-42) including [Java](https://en.wikipedia.org/wiki/Java_(programming_language) "Java (programming language)"), [JavaScript](https://en.wikipedia.org/wiki/JavaScript "JavaScript") (PL/V8), [R](https://en.wikipedia.org/wiki/R_(programming_language) "R (programming language)"), [Ruby](https://en.wikipedia.org/wiki/Ruby_(programming_language) "Ruby (programming language)"), and others.

### Triggers\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=16 "Edit section: Triggers")]

Triggers are events triggered by the action of SQL [data manipulation language](https://en.wikipedia.org/wiki/Data_manipulation_language "Data manipulation language") (DML) statements. For example, an [INSERT](https://en.wikipedia.org/wiki/Insert_(SQL) "Insert (SQL)") statement might activate a trigger that checks if the values of the statement are valid. Most triggers are only activated by either INSERT or [UPDATE](https://en.wikipedia.org/wiki/Update_(SQL) "Update (SQL)") statements.

Triggers are fully supported and can be attached to tables. Triggers can be per-column and conditional, in that UPDATE triggers can target specific columns of a table, and triggers can be told to execute under a set of conditions as specified in the trigger's WHERE clause. Triggers can be attached to [views](https://en.wikipedia.org/wiki/View_(SQL) "View (SQL)") by using the INSTEAD OF condition. Multiple triggers are fired in alphabetical order. In addition to calling functions written in the native PL/pgSQL, triggers can also invoke functions written in other languages like PL/Python or PL/Perl.

### Asynchronous notifications\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=17 "Edit section: Asynchronous notifications")]

PostgreSQL provides an asynchronous messaging system that is accessed through the NOTIFY, LISTEN and UNLISTEN commands. A session can issue a NOTIFY command, along with the user-specified channel and an optional payload, to mark a particular event occurring. Other sessions are able to detect these events by issuing a LISTEN command, which can listen to a particular channel. This functionality can be used for a wide variety of purposes, such as letting other sessions know when a table has updated or for separate applications to detect when a particular action has been performed. Such a system prevents the need for continuous polling by applications to see if anything has yet changed, and reducing unnecessary overhead. Notifications are fully transactional, in that messages are not sent until the transaction they were sent from is committed. This eliminates the problem of messages being sent for an action being performed which is then rolled back.

Many of the connectors for PostgreSQL provide support for this notification system (including libpq, JDBC, Npgsql, psycopg and node.js) so it can be used by external applications.

### Rules\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=18 "Edit section: Rules")]

Rules allow the "query tree" of an incoming query to be rewritten. "Query Re-Write Rules" are attached to a table/class and "Re-Write" the incoming DML (select, insert, update, and/or delete) into one or more queries that either replace the original DML statement or execute in addition to it. Query Re-Write occurs after DML statement parsing, but before query planning.

### Other querying features\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=19 "Edit section: Other querying features")]

-   [Transactions](https://en.wikipedia.org/wiki/Database_transaction "Database transaction")

-   [Full-text search](https://en.wikipedia.org/wiki/Full-text_search "Full-text search")

-   Views

    -   Materialized views[\[43\]](#cite_note-materialized_views-43)
    -   Updateable views[\[44\]](#cite_note-updatable_views-44)
    -   Recursive views[\[45\]](#cite_note-recursive_views-45)

-   Inner, outer (full, left and right), and cross [joins](https://en.wikipedia.org/wiki/Join_(SQL) "Join (SQL)")

-   Sub-[selects](https://en.wikipedia.org/wiki/Select_(SQL) "Select (SQL)")

    -   Correlated sub-queries[\[46\]](#cite_note-Introduction_and_Concepts-46)

-   [Regular expressions](https://en.wikipedia.org/wiki/Regular_expression "Regular expression")[\[47\]](#cite_note-Bernier-47)

-   [common table expressions](https://en.wikipedia.org/wiki/Hierarchical_and_recursive_queries_in_SQL#Common_table_expression "Hierarchical and recursive queries in SQL") and writable common table expressions

-   Encrypted connections via [Transport Layer Security](https://en.wikipedia.org/wiki/Transport_Layer_Security "Transport Layer Security") (TLS); current versions do not use vulnerable SSL, even with that configuration option[\[48\]](#cite_note-POODLE-48)

-   Domains

-   [Savepoints](https://en.wikipedia.org/wiki/Savepoint "Savepoint")

-   [Two-phase commit](https://en.wikipedia.org/wiki/Two-phase_commit_protocol "Two-phase commit protocol")

-   The Oversized-Attribute Storage Technique (TOAST) is used to transparently store large table attributes (such as big MIME attachments or XML messages) in a separate area, with automatic compression.

-   [Embedded SQL](https://en.wikipedia.org/wiki/Embedded_SQL "Embedded SQL") is implemented using preprocessor. SQL code is first written embedded into C code. Then code is run through ECPG preprocessor, which replaces SQL with calls to code library. Then code can be compiled using a C compiler. Embedding works also with [C++](https://en.wikipedia.org/wiki/C%2B%2B "C++") but it does not recognize all C++ constructs.

### Concurrency model\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=20 "Edit section: Concurrency model")]

PostgreSQL server is [process](https://en.wikipedia.org/wiki/Process_(computing) "Process (computing)")-based (not threaded), and uses one operating system process per database session. Multiple sessions are automatically spread across all available CPUs by the operating system. Starting with PostgreSQL 9.6, many types of queries can also be parallelized across multiple background worker processes, taking advantage of multiple CPUs or cores.[\[49\]](#cite_note-49) Client applications can use threads and create multiple database connections from each thread.[\[50\]](#cite_note-50)

## Security\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=21 "Edit section: Security")]

PostgreSQL manages its internal security on a per-[role](https://en.wikipedia.org/wiki/Role-oriented_programming "Role-oriented programming") basis. A role is generally regarded to be a user (a role that can log in), or a group (a role of which other roles are members). Permissions can be granted or revoked on any object down to the column level, and can also allow/prevent the creation of new objects at the database, schema or table levels.

PostgreSQL's SECURITY LABEL feature (extension to SQL standards), allows for additional security; with a bundled loadable module that supports label-based [mandatory access control](https://en.wikipedia.org/wiki/Mandatory_access_control "Mandatory access control") (MAC) based on [Security-Enhanced Linux](https://en.wikipedia.org/wiki/Security-Enhanced_Linux "Security-Enhanced Linux") (SELinux) security policy.[\[51\]](#cite_note-51)[\[52\]](#cite_note-52)

PostgreSQL natively supports a broad number of external authentication mechanisms, including:

-   Password: either [MD5](https://en.wikipedia.org/wiki/MD5 "MD5") or plain-text

-   [Generic Security Services Application Program Interface](https://en.wikipedia.org/wiki/Generic_Security_Services_Application_Program_Interface "Generic Security Services Application Program Interface") (GSSAPI)

-   [Security Support Provider Interface](https://en.wikipedia.org/wiki/Security_Support_Provider_Interface "Security Support Provider Interface") (SSPI)

-   [Kerberos](https://en.wikipedia.org/wiki/Kerberos_(protocol) "Kerberos (protocol)")

-   [ident](https://en.wikipedia.org/wiki/Ident_protocol "Ident protocol") (maps O/S user-name as provided by an ident server to database user-name)

-   Peer (maps local user name to database user name)

-   [Lightweight Directory Access Protocol](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol "Lightweight Directory Access Protocol") (LDAP)

    -   [Active Directory](https://en.wikipedia.org/wiki/Active_Directory "Active Directory") (AD)

-   [RADIUS](https://en.wikipedia.org/wiki/RADIUS "RADIUS")

-   Certificate

-   [Pluggable authentication module](https://en.wikipedia.org/wiki/Pluggable_authentication_module "Pluggable authentication module") (PAM)

The GSSAPI, SSPI, Kerberos, peer, ident and certificate methods can also use a specified "map" file that lists which users matched by that authentication system are allowed to connect as a specific database user.

These methods are specified in the cluster's host-based authentication configuration file (`pg_hba.conf`), which determines what connections are allowed. This allows control over which user can connect to which database, where they can connect from (IP address, IP address range, domain socket), which authentication system will be enforced, and whether the connection must use [Transport Layer Security](https://en.wikipedia.org/wiki/Transport_Layer_Security "Transport Layer Security") (TLS).

## Standards compliance\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=22 "Edit section: Standards compliance")]

PostgreSQL claims high, but not complete, conformance with the SQL standard. One exception is the handling of unquoted identifiers like table or column names. In PostgreSQL they are folded – internal – to lower case characters[\[53\]](#cite_note-identifiers-53) whereas the standard says that unquoted identifiers should be folded to upper case. Thus, `Foo` should be equivalent to `FOO` not `foo` according to the standard.

## Benchmarks and performance\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=23 "Edit section: Benchmarks and performance")]

Many informal performance studies of PostgreSQL have been done.[\[54\]](#cite_note-Berkus-54) Performance improvements aimed at improving scalability began heavily with version 8.1. Simple benchmarks between version 8.0 and version 8.4 showed that the latter was more than 10 times faster on read-only workloads and at least 7.5 times faster on both read and write workloads.[\[55\]](#cite_note-Vilmos-55)

The first industry-standard and peer-validated benchmark was completed in June 2007, using the Sun Java System Application Server (proprietary version of [GlassFish](https://en.wikipedia.org/wiki/GlassFish "GlassFish")) 9.0 Platform Edition, [UltraSPARC T1](https://en.wikipedia.org/wiki/UltraSPARC_T1 "UltraSPARC T1")-based [Sun Fire](https://en.wikipedia.org/wiki/Sun_Fire "Sun Fire") server and PostgreSQL 8.2.[\[56\]](#cite_note-SPECJ-56) This result of 778.14 SPECjAppServer2004 JOPS@Standard compares favourably with the 874 JOPS@Standard with Oracle 10 on an [Itanium](https://en.wikipedia.org/wiki/Itanium "Itanium")-based [HP-UX](https://en.wikipedia.org/wiki/HP-UX "HP-UX") system.[\[54\]](#cite_note-Berkus-54)

In August 2007, Sun submitted an improved benchmark score of 813.73 SPECjAppServer2004 JOPS@Standard. With the [system under test](https://en.wikipedia.org/wiki/System_under_test "System under test") at a reduced price, the price/performance improved from $84.98/JOPS to $70.57/JOPS.[\[57\]](#cite_note-SPECjAppServer2004-57)

The default configuration of PostgreSQL uses only a small amount of dedicated memory for performance-critical purposes such as caching database blocks and sorting. This limitation is primarily because older operating systems required kernel changes to allow allocating large blocks of [shared memory](https://en.wikipedia.org/wiki/Shared_memory "Shared memory").[\[58\]](#cite_note-Kernel_Resources-58) PostgreSQL.org provides advice on basic recommended performance practice in a [wiki](https://en.wikipedia.org/wiki/Wiki "Wiki").[\[59\]](#cite_note-pg9hiperf-59)

In April 2012, Robert Haas of EnterpriseDB demonstrated PostgreSQL 9.2's linear CPU scalability using a server with 64 cores.[\[60\]](#cite_note-Haas-60)

Matloob Khushi performed benchmarking between Postgresql 9.0 and MySQL 5.6.15 for their ability to process genomic data. In his performance analysis he found that PostgreSQL extracts overlapping genomic regions eight times faster than MySQL using two datasets of 80,000 each forming random human DNA regions. Insertion and data uploads in PostgreSQL were also better, although general searching ability of both databases was almost equivalent.[\[61\]](#cite_note-61)

## Platforms\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=24 "Edit section: Platforms")]

PostgreSQL is available for the following operating systems: [Linux](https://en.wikipedia.org/wiki/Linux "Linux") (all recent distributions), 64-bit installers available for [macOS](https://en.wikipedia.org/wiki/MacOS "MacOS") (OS X)[\[13\]](#cite_note-OS_X-13) version 10.6 and newer – [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows "Microsoft Windows") (with installers available for 64-bit version; tested on latest versions and back to [Windows 2012 R2](https://en.wikipedia.org/wiki/Windows_2012_R2 "Windows 2012 R2"),[\[62\]](#cite_note-62) while for PostgreSQL version 10 and older a 32-bit installer is available and tested down to 32-bit [Windows 2008](https://en.wikipedia.org/wiki/Windows_2008 "Windows 2008") R1; compilable by e.g. [Visual Studio](https://en.wikipedia.org/wiki/Microsoft_Visual_Studio "Microsoft Visual Studio"), version 2013 up up to most recent 2019 version) – [FreeBSD](https://en.wikipedia.org/wiki/FreeBSD "FreeBSD"), [OpenBSD](https://en.wikipedia.org/wiki/OpenBSD "OpenBSD"),[\[63\]](#cite_note-openbsd-63) [NetBSD](https://en.wikipedia.org/wiki/NetBSD "NetBSD"), [AIX](https://en.wikipedia.org/wiki/IBM_AIX "IBM AIX"), [HP-UX](https://en.wikipedia.org/wiki/HP-UX "HP-UX"), [Solaris](https://en.wikipedia.org/wiki/Solaris_(operating_system) "Solaris (operating system)"), and [UnixWare](https://en.wikipedia.org/wiki/UnixWare "UnixWare"); and not officially tested: [DragonFly BSD](https://en.wikipedia.org/wiki/DragonFly_BSD "DragonFly BSD"), [BSD/OS](https://en.wikipedia.org/wiki/BSD/OS "BSD/OS"), [IRIX](https://en.wikipedia.org/wiki/IRIX "IRIX"), [OpenIndiana](https://en.wikipedia.org/wiki/OpenIndiana "OpenIndiana"),[\[64\]](#cite_note-OpenIndiana-64) [OpenSolaris](https://en.wikipedia.org/wiki/OpenSolaris "OpenSolaris"), [OpenServer](https://en.wikipedia.org/wiki/OpenServer "OpenServer"), and [Tru64 UNIX](https://en.wikipedia.org/wiki/Tru64_UNIX "Tru64 UNIX"). Most other Unix-like systems could also work; most modern do support.

PostgreSQL works on any of the following [instruction set architectures](https://en.wikipedia.org/wiki/Instruction_set_architecture "Instruction set architecture"): [x86](https://en.wikipedia.org/wiki/X86 "X86") and [x86-64](https://en.wikipedia.org/wiki/X86-64 "X86-64") on Windows and other operating systems; these are supported on other than Windows: IA-64 [Itanium](https://en.wikipedia.org/wiki/Itanium "Itanium") (external support for HP-UX), [PowerPC](https://en.wikipedia.org/wiki/PowerPC "PowerPC"), PowerPC 64, [S/390](https://en.wikipedia.org/wiki/IBM_System/390 "IBM System/390"), [S/390x](https://en.wikipedia.org/wiki/IBM_System_z "IBM System z"), [SPARC](https://en.wikipedia.org/wiki/SPARC "SPARC"), SPARC 64, [ARMv8](https://en.wikipedia.org/wiki/ARMv8 "ARMv8")-A ([64-bit](https://en.wikipedia.org/wiki/64-bit_computing "64-bit computing"))[\[65\]](#cite_note-AArch64-65) and older [ARM](https://en.wikipedia.org/wiki/ARM_architecture "ARM architecture") ([32-bit](https://en.wikipedia.org/wiki/32-bit "32-bit"), including older such as [ARMv6](https://en.wikipedia.org/wiki/ARMv6 "ARMv6") in [Raspberry Pi](https://en.wikipedia.org/wiki/Raspberry_Pi "Raspberry Pi")[\[66\]](#cite_note-raspi-66)), [MIPS](https://en.wikipedia.org/wiki/MIPS_architecture "MIPS architecture"), MIPSel, and [PA-RISC](https://en.wikipedia.org/wiki/PA-RISC "PA-RISC"). It was also known to work, but not tested in a while, on [Alpha](https://en.wikipedia.org/wiki/DEC_Alpha "DEC Alpha") (dropped in 9.5), [M68k](https://en.wikipedia.org/wiki/Motorola_68000_series "Motorola 68000 series"), [M32R](https://en.wikipedia.org/wiki/M32R "M32R"), [NS32k](https://en.wikipedia.org/wiki/NS320xx "NS320xx"), and [VAX](https://en.wikipedia.org/wiki/VAX "VAX"). Beyond these, it is possible to build PostgreSQL for an unsupported CPU by disabling [spinlocks](https://en.wikipedia.org/wiki/Spinlock "Spinlock").[\[67\]](#cite_note-SupportedPlatforms-67)

## Database administration\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=25 "Edit section: Database administration")]

See also: [Comparison of database tools](https://en.wikipedia.org/wiki/Comparison_of_database_tools "Comparison of database tools")

Open source front-ends and tools for administering PostgreSQL include:

-   psql

    The primary [front-end](https://en.wikipedia.org/wiki/Front_and_back_ends "Front and back ends") for PostgreSQL is the `psql` [command-line program](https://en.wikipedia.org/wiki/Command-line_program "Command-line program"), which can be used to enter SQL queries directly, or execute them from a file. In addition, psql provides a number of meta-commands and various shell-like features to facilitate writing scripts and automating a wide variety of tasks; for example tab completion of object names and SQL syntax.

-   pgAdmin

    The pgAdmin package is a free and open-source [graphical user interface](https://en.wikipedia.org/wiki/Graphical_user_interface "Graphical user interface") (GUI) administration tool for PostgreSQL, which is supported on many computer platforms.[\[68\]](#cite_note-pgAdmin-68) The program is available in more than a dozen languages. The first prototype, named pgManager, was written for PostgreSQL 6.3.2 from 1998, and rewritten and released as pgAdmin under the GNU General Public License (GPL) in later months. The second incarnation (named pgAdmin II) was a complete rewrite, first released on January 16, 2002. The third version, pgAdmin III, was originally released under the [Artistic License](https://en.wikipedia.org/wiki/Artistic_License "Artistic License") and then released under the same license as PostgreSQL. Unlike prior versions that were written in [Visual Basic](https://en.wikipedia.org/wiki/Visual_Basic "Visual Basic"), pgAdmin III is written in C++, using the [wxWidgets](https://en.wikipedia.org/wiki/WxWidgets "WxWidgets")[\[69\]](#cite_note-69) framework allowing it to run on most common operating systems. The query tool includes a scripting language called pgScript for supporting admin and development tasks. In December 2014, Dave Page, the pgAdmin project founder and primary developer,[\[70\]](#cite_note-70) announced that with the shift towards web-based models, work has begun on pgAdmin 4 with the aim to facilitate cloud deployments.[\[71\]](#cite_note-71) In 2016, pgAdmin 4 was released. pgAdmin 4 backend was written in [Python](https://en.wikipedia.org/wiki/Python_(programming_language) "Python (programming language)"), using Flask and [Qt framework](https://en.wikipedia.org/wiki/Qt_(software) "Qt (software)").[\[72\]](#cite_note-72)

-   phpPgAdmin

    phpPgAdmin is a web-based administration tool for PostgreSQL written in PHP and based on the popular [phpMyAdmin](https://en.wikipedia.org/wiki/PhpMyAdmin "PhpMyAdmin") interface originally written for [MySQL](https://en.wikipedia.org/wiki/MySQL "MySQL") administration.[\[73\]](#cite_note-PHPADMIN-73)

-   PostgreSQL Studio

    PostgreSQL Studio allows users to perform essential PostgreSQL database development tasks from a web-based console. PostgreSQL Studio allows users to work with cloud databases without the need to open firewalls.[\[74\]](#cite_note-POSTGRESQLSTUDIO-74)

-   TeamPostgreSQL

    AJAX/JavaScript-driven web interface for PostgreSQL. Allows browsing, maintaining and creating data and database objects via a web browser. The interface offers tabbed SQL editor with autocompletion, row editing widgets, click-through foreign key navigation between rows and tables, _favorites_ management for commonly used scripts, among other features. Supports SSH for both the web interface and the [database connections](https://en.wikipedia.org/wiki/Database_connection "Database connection"). Installers are available for Windows, Macintosh, and Linux, and a simple cross-platform archive that runs from a script.[\[75\]](#cite_note-TEAMPOSTGRESQL-75)

-   LibreOffice, OpenOffice.org

    [LibreOffice](https://en.wikipedia.org/wiki/LibreOffice "LibreOffice") and [OpenOffice.org](https://en.wikipedia.org/wiki/OpenOffice.org "OpenOffice.org") Base can be used as a front-end for PostgreSQL.[\[76\]](#cite_note-ooAsFrntEnd-76)[\[77\]](#cite_note-loAsFrntEnd-77)

-   pgBadger

    The pgBadger PostgreSQL log analyzer generates detailed reports from a PostgreSQL log file.[\[78\]](#cite_note-tuningPGinstance-78)

-   pgDevOps

    pgDevOps is a suite of web tools to install & manage multiple PostgreSQL versions, extensions, and community components, develop SQL queries, monitor running databases and find performance problems.[\[79\]](#cite_note-79)

A number of companies offer proprietary tools for PostgreSQL. They often consist of a universal core that is adapted for various specific database products. These tools mostly share the administration features with the open source tools but offer improvements in [data modeling](https://en.wikipedia.org/wiki/Data_modeling "Data modeling"), importing, exporting or reporting.

## Notable users\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=26 "Edit section: Notable users")]

Notable organizations and products that use PostgreSQL as the primary database include:

-   In 2009, the social-networking website [Myspace](https://en.wikipedia.org/wiki/Myspace "Myspace") used [Aster Data Systems](https://en.wikipedia.org/wiki/Aster_Data_Systems "Aster Data Systems")'s nCluster database for data warehousing, which was built on unmodified PostgreSQL.[\[80\]](#cite_note-Cecchet-80)[\[81\]](#cite_note-Aster_Data-81)
-   [Geni.com](https://en.wikipedia.org/wiki/Geni.com "Geni.com") uses PostgreSQL for their main genealogy database.[\[82\]](#cite_note-Geni-82)
-   [OpenStreetMap](https://en.wikipedia.org/wiki/OpenStreetMap "OpenStreetMap"), a collaborative project to create a free editable map of the world.[\[83\]](#cite_note-OpenStreetMap-83)
-   [Afilias](https://en.wikipedia.org/wiki/Afilias "Afilias"), domain registries for [.org](https://en.wikipedia.org/wiki/.org ".org"), [.info](https://en.wikipedia.org/wiki/.info ".info") and others.[\[84\]](#cite_note-Afilias-84)[\[85\]](#cite_note-begPHPpg-book-85)
-   [Sony Online](https://en.wikipedia.org/wiki/Sony_Online "Sony Online") multiplayer online games.[\[86\]](#cite_note-Sony_Online-86)
-   [BASF](https://en.wikipedia.org/wiki/BASF "BASF"), shopping platform for their agribusiness portal.[\[87\]](#cite_note-BASF-87)
-   [Reddit](https://en.wikipedia.org/wiki/Reddit "Reddit") social news website.[\[88\]](#cite_note-Reddit-88)
-   [Skype](https://en.wikipedia.org/wiki/Skype "Skype") VoIP application, central [business](https://en.wikipedia.org/wiki/Business "Business") databases.[\[89\]](#cite_note-89)
-   [Sun xVM](https://en.wikipedia.org/wiki/Sun_xVM "Sun xVM"), Sun's virtualization and datacenter automation suite.[\[90\]](#cite_note-xVM-90)
-   [MusicBrainz](https://en.wikipedia.org/wiki/MusicBrainz "MusicBrainz"), open online music encyclopedia.[\[91\]](#cite_note-MusicBrainz-91)
-   The [International Space Station](https://en.wikipedia.org/wiki/International_Space_Station "International Space Station") – to collect telemetry data in orbit and replicate it to the ground.[\[92\]](#cite_note-ISS-92)
-   [MyYearbook](https://en.wikipedia.org/wiki/MyYearbook "MyYearbook") social-networking site.[\[93\]](#cite_note-MyYearbook-93)
-   [Instagram](https://en.wikipedia.org/wiki/Instagram "Instagram"), a mobile photo-sharing service.[\[94\]](#cite_note-Instagram-94)
-   [Disqus](https://en.wikipedia.org/wiki/Disqus "Disqus"), an online discussion and commenting service.[\[95\]](#cite_note-Disqus-95)
-   [TripAdvisor](https://en.wikipedia.org/wiki/TripAdvisor "TripAdvisor"), travel-information website of mostly user-generated content.[\[96\]](#cite_note-TripAdvisor-96)
-   [Yandex](https://en.wikipedia.org/wiki/Yandex "Yandex"), a Russian internet company switched its Yandex.Mail service from Oracle to Postgres.[\[97\]](#cite_note-97)
-   [Amazon Redshift](https://en.wikipedia.org/wiki/Amazon_Redshift "Amazon Redshift"), part of AWS, a columnar [online analytical processing](https://en.wikipedia.org/wiki/Online_analytical_processing "Online analytical processing") (OLAP) system based on [ParAccel](https://en.wikipedia.org/wiki/ParAccel "ParAccel")'s Postgres modifications.
-   [National Oceanic and Atmospheric Administration](https://en.wikipedia.org/wiki/National_Oceanic_and_Atmospheric_Administration "National Oceanic and Atmospheric Administration")'s (NOAA) [National Weather Service](https://en.wikipedia.org/wiki/National_Weather_Service "National Weather Service") (NWS), Interactive Forecast Preparation System (IFPS), a system that integrates data from the [NEXRAD](https://en.wikipedia.org/wiki/NEXRAD "NEXRAD") [weather radars](https://en.wikipedia.org/wiki/Weather_radar "Weather radar"), surface, and [hydrology](https://en.wikipedia.org/wiki/Hydrology "Hydrology") systems to build detailed localized forecast models.[\[85\]](#cite_note-begPHPpg-book-85)[\[98\]](#cite_note-pg9AdminCookEdt2-book-98)
-   [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom")'s national weather service, [Met Office](https://en.wikipedia.org/wiki/Met_Office "Met Office"), has begun swapping Oracle for PostgreSQL in a strategy to deploy more open source technology.[\[98\]](#cite_note-pg9AdminCookEdt2-book-98)[\[99\]](#cite_note-99)
-   [WhitePages.com](https://en.wikipedia.org/wiki/WhitePages.com "WhitePages.com") had been using Oracle[\[100\]](#cite_note-100)\[_[circular reference](https://en.wikipedia.org/wiki/Wikipedia:Verifiability#Wikipedia_and_sources_that_mirror_or_use_it "Wikipedia:Verifiability")_] and [MySQL](https://en.wikipedia.org/wiki/MySQL "MySQL"), but when it came to moving its core directories in-house, it turned to PostgreSQL. Because WhitePages.com needs to combine large sets of data from multiple sources, PostgreSQL's ability to load and index data at high rates was a key to its decision to use PostgreSQL.[\[85\]](#cite_note-begPHPpg-book-85)
-   [FlightAware](https://en.wikipedia.org/wiki/FlightAware "FlightAware"), a flight tracking website.[\[101\]](#cite_note-101)
-   [Grofers](https://en.wikipedia.org/wiki/Grofers "Grofers"), an online grocery delivery service.[\[102\]](#cite_note-102)
-   _[The Guardian](https://en.wikipedia.org/wiki/The_Guardian "The Guardian")_ migrated from [MongoDB](https://en.wikipedia.org/wiki/MongoDB "MongoDB") to PostgreSQL in 2018.[\[103\]](#cite_note-103)

## Service implementations\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=27 "Edit section: Service implementations")]

Some notable vendors offer PostgreSQL as [software as a service](https://en.wikipedia.org/wiki/Software_as_a_service "Software as a service"):

-   [Heroku](https://en.wikipedia.org/wiki/Heroku "Heroku"), a [platform as a service](https://en.wikipedia.org/wiki/Platform_as_a_service "Platform as a service") provider, has supported PostgreSQL since the start in 2007.[\[104\]](#cite_note-Heroku-104) They offer value-add features like full database _roll-back_ (ability to restore a database from any specified time),[\[105\]](#cite_note-Darrow-105) which is based on WAL-E, open-source software developed by Heroku.[\[106\]](#cite_note-Kerstiens-106)
-   In January 2012, [EnterpriseDB](https://en.wikipedia.org/wiki/EnterpriseDB "EnterpriseDB") released a cloud version of both PostgreSQL and their own proprietary Postgres Plus Advanced Server with automated provisioning for failover, replication, load-balancing, and scaling. It runs on [Amazon Web Services](https://en.wikipedia.org/wiki/Amazon_Web_Services "Amazon Web Services").[\[107\]](#cite_note-Techweekeurope-107)
-   [VMware](https://en.wikipedia.org/wiki/VMware "VMware") has offered vFabric Postgres (also termed vPostgres[\[108\]](#cite_note-108)) for private clouds on [VMware vSphere](https://en.wikipedia.org/wiki/VMware_vSphere "VMware vSphere") since May 2012.[\[109\]](#cite_note-Sargent-109)
-   In November 2013, [Amazon Web Services](https://en.wikipedia.org/wiki/Amazon_Web_Services "Amazon Web Services") announced the addition of PostgreSQL to their [Relational Database Service](https://en.wikipedia.org/wiki/Amazon_Relational_Database_Service "Amazon Relational Database Service") offering.[\[110\]](#cite_note-aws.typepad.com-110)[\[111\]](#cite_note-Williams-111)
-   In November 2016, [Amazon Web Services](https://en.wikipedia.org/wiki/Amazon_Web_Services "Amazon Web Services") announced the addition of PostgreSQL compatibility to their cloud-native [Amazon Aurora](https://en.wikipedia.org/wiki/Amazon_Aurora "Amazon Aurora") managed database offering.[\[112\]](#cite_note-112)
-   In May 2017, [Microsoft Azure](https://en.wikipedia.org/wiki/Microsoft_Azure "Microsoft Azure") announced Azure Databases for PostgreSQL[\[113\]](#cite_note-113)
-   In July 2019, [ScaleGrid](https://scalegrid.io/postgresql.html) announced fully managed PostgreSQL hosting on AWS and Azure.[\[114\]](#cite_note-114)

## Release history\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=28 "Edit section: Release history")]

| Release | First release                       | Latest minor version                       | Latest release | End of  
life[\[115\]](#cite_note-115) | Milestones                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ------- | ----------------------------------- | ------------------------------------------ | -------------- | -------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 6.0     | 1997-01-29                          | N/A                                        | N/A            | N/A                                    | First formal release of PostgreSQL, unique indexes, pg_dumpall utility, ident authentication                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 6.1     | 1997-06-08                          | Old version, no longer supported: 6.1.1    | 1997-07-22     | N/A                                    | Multicolumn indexes, sequences, money data type, GEQO (GEnetic Query Optimizer)                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| 6.2     | 1997-10-02                          | Old version, no longer supported: 6.2.1    | 1997-10-17     | N/A                                    | JDBC interface, triggers, server programming interface, constraints                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| 6.3     | 1998-03-01                          | Old version, no longer supported: 6.3.2    | 1998-04-07     | 2003-03-01                             | SQL-92 subselect ability, PL/pgTCL                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| 6.4     | 1998-10-30                          | Old version, no longer supported: 6.4.2    | 1998-12-20     | 2003-10-30                             | VIEWs (then only read-only) and RULEs, [PL/pgSQL](https://en.wikipedia.org/wiki/PL/pgSQL "PL/pgSQL")                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 6.5     | 1999-06-09                          | Old version, no longer supported: 6.5.3    | 1999-10-13     | 2004-06-09                             | [MVCC](https://en.wikipedia.org/wiki/Multiversion_concurrency_control "Multiversion concurrency control"), temporary tables, more SQL statement support (CASE, INTERSECT, and EXCEPT)                                                                                                                                                                                                                                                                                                                                                          |
| 7.0     | 2000-05-08                          | Old version, no longer supported: 7.0.3    | 2000-11-11     | 2004-05-08                             | Foreign keys, SQL-92 syntax for joins                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| 7.1     | 2001-04-13                          | Old version, no longer supported: 7.1.3    | 2001-08-15     | 2006-04-13                             | Write-ahead log, outer joins                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 7.2     | 2002-02-04                          | Old version, no longer supported: 7.2.8    | 2005-05-09     | 2007-02-04                             | PL/Python, [OIDs](https://en.wikipedia.org/wiki/Object_identifier "Object identifier") no longer required, [internationalization](https://en.wikipedia.org/wiki/Internationalization_and_localization "Internationalization and localization") of messages                                                                                                                                                                                                                                                                                     |
| 7.3     | 2002-11-27                          | Old version, no longer supported: 7.3.21   | 2008-01-07     | 2007-11-27                             | Schema, table function, [prepared query](https://en.wikipedia.org/wiki/Prepared_query "Prepared query")[\[116\]](#cite_note-116)                                                                                                                                                                                                                                                                                                                                                                                                               |
| 7.4     | 2003-11-17                          | Old version, no longer supported: 7.4.30   | 2010-10-04     | 2010-10-01                             | Optimization on JOINs and [data warehouse](https://en.wikipedia.org/wiki/Data_warehouse "Data warehouse") functions[\[117\]](#cite_note-117)                                                                                                                                                                                                                                                                                                                                                                                                   |
| 8.0     | 2005-01-19                          | Old version, no longer supported: 8.0.26   | 2010-10-04     | 2010-10-01                             | Native server on [Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows "Microsoft Windows"), [savepoints](https://en.wikipedia.org/wiki/Savepoint "Savepoint"), [tablespaces](https://en.wikipedia.org/wiki/Tablespace "Tablespace"), [point-in-time recovery](https://en.wikipedia.org/wiki/Point-in-time_recovery "Point-in-time recovery")[\[118\]](#cite_note-118)                                                                                                                                                           |
| 8.1     | 2005-11-08                          | Old version, no longer supported: 8.1.23   | 2010-12-16     | 2010-11-08                             | Performance optimization, two-phase commit, table [partitioning](https://en.wikipedia.org/wiki/Partition_(database) "Partition (database)"), index bitmap scan, shared row locking, roles                                                                                                                                                                                                                                                                                                                                                      |
| 8.2     | 2006-12-05                          | Old version, no longer supported: 8.2.23   | 2011-12-05     | 2011-12-05                             | Performance optimization, online index builds, advisory locks, warm standby[\[119\]](#cite_note-119)                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 8.3     | 2008-02-04                          | Old version, no longer supported: 8.3.23   | 2013-02-07     | 2013-02-07                             | Heap-only tuples, [full text search](https://en.wikipedia.org/wiki/Full_text_search "Full text search"),[\[120\]](#cite_note-120) [SQL/XML](https://en.wikipedia.org/wiki/SQL/XML "SQL/XML"), ENUM types, [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier "Universally unique identifier") types                                                                                                                                                                                                                            |
| 8.4     | 2009-07-01                          | Old version, no longer supported: 8.4.22   | 2014-07-24     | 2014-07-24                             | Windowing functions, column-level permissions, parallel database restore, per-database collation, [common table expressions](https://en.wikipedia.org/wiki/Common_table_expressions "Common table expressions") and recursive queries[\[121\]](#cite_note-121)                                                                                                                                                                                                                                                                                 |
| 9.0     | 2010-09-20                          | Old version, no longer supported: 9.0.23   | 2015-10-08     | 2015-10-08                             | Built-in binary streaming [replication](https://en.wikipedia.org/wiki/Replication_(computing) "Replication (computing)"), [hot standby](https://en.wikipedia.org/wiki/Hot_standby "Hot standby"), in-place upgrade ability, 64-bit Windows[\[122\]](#cite_note-122)                                                                                                                                                                                                                                                                            |
| 9.1     | 2011-09-12                          | Old version, no longer supported: 9.1.24   | 2016-10-27     | 2016-10-27                             | [Synchronous replication](https://en.wikipedia.org/wiki/Synchronous_replication "Synchronous replication"), per-column [collations](https://en.wikipedia.org/wiki/Collation "Collation"), unlogged tables, [serializable snapshot isolation](https://en.wikipedia.org/wiki/Serializable_snapshot_isolation "Serializable snapshot isolation"), writeable common table expressions, [SELinux](https://en.wikipedia.org/wiki/Security-Enhanced_Linux "Security-Enhanced Linux") integration, extensions, foreign tables[\[123\]](#cite_note-123) |
| 9.2     | 2012-09-10[\[124\]](#cite_note-124) | Old version, no longer supported: 9.2.24   | 2017-11-09     | 2017-11-09                             | Cascading streaming replication, index-only scans, native [JSON](https://en.wikipedia.org/wiki/JSON "JSON") support, improved lock management, range types, pg_receivexlog tool, space-partitioned GiST indexes                                                                                                                                                                                                                                                                                                                                |
| 9.3     | 2013-09-09                          | Old version, no longer supported: 9.3.25   | 2018-11-08     | 2018-11-08                             | Custom background workers, data checksums, dedicated JSON operators, LATERAL JOIN, faster pg_dump, new pg_isready server monitoring tool, trigger features, view features, writeable foreign tables, [materialized views](https://en.wikipedia.org/wiki/Materialized_view "Materialized view"), replication improvements                                                                                                                                                                                                                       |
| 9.4     | 2014-12-18                          | Older version, yet still supported: 9.4.24 | 2019-08-08     | 2020-02-13                             | JSONB data type, ALTER SYSTEM statement for changing config values, ability to refresh materialized views without blocking reads, dynamic registration/start/stop of background worker processes, Logical Decoding API, GiN index improvements, Linux huge page support, database cache reloading via pg_prewarm, reintroducing Hstore as the column type of choice for document-style data.[\[125\]](#cite_note-125)                                                                                                                          |
| 9.5     | 2016-01-07                          | Older version, yet still supported: 9.5.19 | 2019-08-08     | 2021-02-11                             | UPSERT, row level security, TABLESAMPLE, CUBE/ROLLUP, GROUPING SETS, and new [BRIN](https://en.wikipedia.org/wiki/Block_Range_Index "Block Range Index") index[\[126\]](#cite_note-126)                                                                                                                                                                                                                                                                                                                                                        |
| 9.6     | 2016-09-29                          | Older version, yet still supported: 9.6.15 | 2019-08-08     | 2021-11-11                             | Parallel query support, PostgreSQL foreign data wrapper (FDW) improvements with sort/join pushdown, multiple synchronous standbys, faster vacuuming of large table                                                                                                                                                                                                                                                                                                                                                                             |
| 10      | 2017-10-05                          | Older version, yet still supported: 10.10  | 2019-08-08     | 2022-11-10                             | Logical replication,[\[127\]](#cite_note-127) declarative table partitioning, improved query parallelism                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 11      | 2018-10-18                          | Older version, yet still supported: 11.5   | 2019-08-08     | 2023-11-09                             | Increased robustness and performance for partitioning, transactions supported in stored procedures, enhanced abilities for query parallelism, just-in-time (JIT) compiling for expressions[\[128\]](#cite_note-128)[\[129\]](#cite_note-129)                                                                                                                                                                                                                                                                                                   |
| 12      | 2019-10-03                          | Current stable version: **12.0**           | 2019-10-03     | 2024-11-14                             | Improvements to query performance and space utilization; SQL/JSON path expression support; generated columns; improvements to internationalization, and authentication; new pluggable table storage interface.[\[130\]](#cite_note-130)                                                                                                                                                                                                                                                                                                        |

**Legend:**

Old version

Older version, still supported

**Latest version**

Latest preview version

Future release

![](PostgreSQL%20-%20Wikipedia_files/986746e0460ee0493ffa9b3cd9d0d025.png)

## See also\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=29 "Edit section: See also")]

-   ![](PostgreSQL%20-%20Wikipedia_files/28px-Free_and_open-source_software_logo_2009.png)[Free and open-source software portal](https://en.wikipedia.org/wiki/Portal:Free_and_open-source_software "Portal:Free and open-source software")


-   [Comparison of relational database management systems](https://en.wikipedia.org/wiki/Comparison_of_relational_database_management_systems "Comparison of relational database management systems")
-   [Database scalability](https://en.wikipedia.org/wiki/Database_scalability "Database scalability")
-   [List of databases using MVCC](https://en.wikipedia.org/wiki/List_of_databases_using_MVCC "List of databases using MVCC")
-   [SQL compliance](https://en.wikipedia.org/wiki/SQL_compliance "SQL compliance")

## References\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=30 "Edit section: References")]

1.  **[^](#cite_ref-1 "Jump up")** ["PostgreSQL"](https://www.postgresql.org/). Retrieved September 21, 2019. "PostgreSQL: The World's Most Advanced Open Source Relational Database"
2.  ^ [Jump up to:_**a**_](#cite_ref-birthday_2-0) [_**b**_](#cite_ref-birthday_2-1) ["Happy Birthday, PostgreSQL!"](https://www.postgresql.org/about/news/978/). PostgreSQL Global Development Group. July 8, 2008.
3.  **[^](#cite_ref-release_3-0 "Jump up")** ["PostgreSQL 12 Released!"](https://www.postgresql.org/about/news/1976/). _PostgreSQL_. The PostgreSQL Global Development Group. October 3, 2019. Retrieved October 3, 2019.
4.  **[^](#cite_ref-4 "Jump up")** ["PostgreSQL: Downloads"](https://www.postgresql.org/download/). Retrieved April 12, 2019.
5.  ^ [Jump up to:_**a**_](#cite_ref-about/licence_5-0) [_**b**_](#cite_ref-about/licence_5-1) ["License"](https://www.postgresql.org/about/licence). PostgreSQL Global Development Group. Retrieved September 20, 2010.
6.  **[^](#cite_ref-approved_by_OSI_6-0 "Jump up")** ["PostgreSQL licence approved by OSI"](http://www.crynwr.com/cgi-bin/ezmlm-cgi?17:mmp:969). Crynwr. February 18, 2010. Retrieved February 18, 2010.
7.  ^ [Jump up to:_**a**_](#cite_ref-OSI_7-0) [_**b**_](#cite_ref-OSI_7-1) ["OSI PostgreSQL Licence"](http://www.opensource.org/licenses/postgresql). Open Source Initiative. February 20, 2010. Retrieved February 20, 2010.
8.  **[^](#cite_ref-8 "Jump up")** ["Debian -- Details of package postgresql in sid"](https://packages.debian.org/sid/postgresql). _debian.org_.
9.  **[^](#cite_ref-9 "Jump up")** ["Licensing:Main"](https://fedoraproject.org/wiki/Licensing:Main?rd=Licensing). _FedoraProject_.
10. **[^](#cite_ref-10 "Jump up")** ["PostgreSQL"](http://directory.fsf.org/wiki/PostgreSQL). _fsf.org_.
11. **[^](#cite_ref-OS_X_Lion_Server_11-0 "Jump up")** ["OS X Lion Server — Technical Specifications"](http://support.apple.com/kb/SP630). August 4, 2011. Retrieved November 12, 2011. "Web Hosting \[..] PostgreSQL"
12. **[^](#cite_ref-MySQL_not_included_12-0 "Jump up")** ["Lion Server: MySQL not included"](http://support.apple.com/kb/HT4828). August 4, 2011. Retrieved November 12, 2011.
13. ^ [Jump up to:_**a**_](#cite_ref-OS_X_13-0) [_**b**_](#cite_ref-OS_X_13-1) ["Mac OS X packages"](https://www.postgresql.org/download/macosx/). The PostgreSQL Global Development Group. Retrieved August 27, 2016.
14. **[^](#cite_ref-intro-whatis_14-0 "Jump up")** ["What is PostgreSQL?"](https://www.postgresql.org/docs/current/static/intro-whatis.html). _PostgreSQL 9.3.0 Documentation_. PostgreSQL Global Development Group. Retrieved September 20, 2013.
15. ^ [Jump up to:_**a**_](#cite_ref-contributors_15-0) [_**b**_](#cite_ref-contributors_15-1) ["Contributor Profiles"](https://www.postgresql.org/community/contributors/). PostgreSQL Global Development Group. Retrieved March 14, 2017.
16. **[^](#cite_ref-Audio_sample_16-0 "Jump up")** ["Audio sample, 5.6k MP3"](https://www.postgresql.org/files/postgresql.mp3).
17. ^ [Jump up to:_**a**_](#cite_ref-design_17-0) [_**b**_](#cite_ref-design_17-1) Stonebraker, M.; Rowe, L. A. (May 1986). [_The design of POSTGRES_](http://db.cs.berkeley.edu/papers/ERL-M85-95.pdf) (PDF). Proc. 1986 ACM [SIGMOD](https://en.wikipedia.org/wiki/SIGMOD "SIGMOD") Conference on Management of Data. Washington, DC. Retrieved December 17, 2011.
18. **[^](#cite_ref-about/history_18-0 "Jump up")** ["PostgreSQL: History"](https://web.archive.org/web/20170326020245/https://www.postgresql.org/about/history/#). PostgreSQL Global Development Group. Archived from [the original](https://www.postgresql.org/about/history/) on March 26, 2017. Retrieved August 27, 2016.
19. **[^](#cite_ref-Project_name_19-0 "Jump up")** ["Project name – statement from the core team"](http://archives.postgresql.org/pgsql-advocacy/2007-11/msg00109.php). archives.postgresql.org. November 16, 2007. Retrieved November 16, 2007.
20. **[^](#cite_ref-20 "Jump up")** ["Michael Stonebraker – A.M. Turing Award Winner"](https://amturing.acm.org/award_winners/stonebraker_1172121.cfm). _amturing.acm.org_. Retrieved March 20, 2018. "Techniques pioneered in Postgres were widely implemented \[..] Stonebraker is the only Turing award winner to have engaged in serial entrepreneurship on anything like this scale, giving him a distinctive perspective on the academic world."
21. **[^](#cite_ref-Stonebraker_21-0 "Jump up")** Stonebraker, M.; Rowe, L. A. [_The POSTGRES data model_](http://db.cs.berkeley.edu/papers/ERL-M87-13.pdf) (PDF). Proceedings of the 13th International Conference on Very Large Data Bases. Brighton, England: Morgan Kaufmann Publishers. pp. 83–96. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [0-934613-46-X](https://en.wikipedia.org/wiki/Special:BookSources/0-934613-46-X "Special:BookSources/0-934613-46-X").
22. **[^](#cite_ref-pavel-history_22-0 "Jump up")** Pavel Stehule (June 9, 2012). ["Historie projektu PostgreSQL"](http://postgres.cz/wiki/Historie_projektu_PostgreSQL) (in Czech).
23. **[^](#cite_ref-University_POSTGRES_23-0 "Jump up")** ["University POSTGRES, Version 4.2"](http://db.cs.berkeley.edu/postgres.html). July 26, 1999.
24. **[^](#cite_ref-20th_anniversary_24-0 "Jump up")** Page, Dave (April 7, 2015). ["Re: 20th anniversary of PostgreSQL ?"](https://www.postgresql.org/message-id/CA+OCxozS_cuaLw=nfS=GdJZmS7ygjhdtZbqVt17wPLfCOtFY4g@mail.gmail.com). _pgsql-advocacy_ (Mailing list). Retrieved April 9, 2015.
25. **[^](#cite_ref-ports_25-0 "Jump up")** Dan R. K. Ports; Kevin Grittner (2012). ["Serializable Snapshot Isolation in PostgreSQL"](http://drkp.net/drkp/papers/ssi-vldb12.pdf) (PDF). _Proceedings of the VLDB Endowment_. **5** (12): 1850–1861. [arXiv](https://en.wikipedia.org/wiki/ArXiv "ArXiv"):[1208.4179](https://arxiv.org/abs/1208.4179). [Bibcode](https://en.wikipedia.org/wiki/Bibcode "Bibcode"):[2012arXiv1208.4179P](https://ui.adsabs.harvard.edu/abs/2012arXiv1208.4179P). [doi](https://en.wikipedia.org/wiki/Digital_object_identifier "Digital object identifier"):[10.14778/2367502.2367523](https://doi.org/10.14778%2F2367502.2367523).
26. **[^](#cite_ref-H_Online_26-0 "Jump up")** [_PostgreSQL 9.1 with synchronous replication_](http://www.h-online.com/open/news/item/PostgreSQL-9-1-with-synchronous-replication-1341228.html) (news), H Online
27. **[^](#cite_ref-Postgres-XC_27-0 "Jump up")** ["Postgres-XC project page"](https://web.archive.org/web/20120701122448/http://postgres-xc.sourceforge.net/) (website). Postgres-XC. Archived from [the original](http://postgres-xc.sourceforge.net/) on July 1, 2012.
28. **[^](#cite_ref-postgres-r_28-0 "Jump up")** ["Postgres-R: a database replication system for PostgreSQL"](http://www.postgres-r.org/). Postgres Global Development Group. Retrieved August 27, 2016.
29. **[^](#cite_ref-bdr_29-0 "Jump up")** ["Postgres-BDR"](http://2ndquadrant.com/en/resources/bdr/). 2ndQuadrant Ltd. Retrieved August 27, 2016.
30. **[^](#cite_ref-Fischer_30-0 "Jump up")** Marit Fischer (November 10, 2007). ["Backcountry.com finally gives something back to the open source community"](https://web.archive.org/web/20101226124550/http://www.backcountrycorp.com/corporate/section/3/press/a511/Backcountry-finally-gives-something-back-to-the-open-source-community.html) (Press release). Backcountry.com. Archived from [the original](http://www.backcountrycorp.com/corporate/section/3/press/a511/Backcountry-finally-gives-something-back-to-the-open-source-community.html) on December 26, 2010.
31. **[^](#cite_ref-SP-GiST_31-0 "Jump up")** Bartunov, O; Sigaev, T (May 2011). [_SP-GiST – a new indexing framework for PostgreSQL_](http://www.pgcon.org/2011/schedule/attachments/197_pgcon-2011.pdf) (PDF). PGCon 2011. Ottawa, Canada. Retrieved January 31, 2016.
32. **[^](#cite_ref-KNN-GiST_32-0 "Jump up")** Bartunov, O; Sigaev, T (May 2010). [_K-nearest neighbour search for PostgreSQL_](http://www.pgcon.org/2010/schedule/attachments/168_pgcon-2010-1.pdf) (PDF). PGCon 2010. Ottawa, Canada. Retrieved January 31, 2016.
33. **[^](#cite_ref-33 "Jump up")** ["PostgreSQL, the NoSQL Database | Linux Journal"](https://www.linuxjournal.com/content/postgresql-nosql-database). _www.linuxjournal.com_.
34. **[^](#cite_ref-jsonb_34-0 "Jump up")** Geoghegan, Peter (March 23, 2014). ["What I think of jsonb"](http://pgeoghegan.blogspot.com/2014/03/what-i-think-of-jsonb.html).
35. **[^](#cite_ref-35 "Jump up")** Obe, Regina; Hsu, Leo S. (2012). "10: Replication and External Data". [_PostgreSQL: Up and Running_](https://books.google.com/books?id=Q8jkIZkMTPcC) (1 ed.). Sebastopol, CA: [O'Reilly Media, Inc.](https://en.wikipedia.org/wiki/O%27Reilly_Media "O'Reilly Media") p. 129. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-4493-2633-3](https://en.wikipedia.org/wiki/Special:BookSources/978-1-4493-2633-3 "Special:BookSources/978-1-4493-2633-3"). Retrieved October 17, 2016. "Foreign Data Wrappers (FDW) \[...] are mechanisms of querying external datasources. PostgreSQL 9.1 introduced this [SQL/MED](https://en.wikipedia.org/wiki/SQL/MED "SQL/MED") standards compliant feature."
36. **[^](#cite_ref-psycopg2_36-0 "Jump up")** ["PostgreSQL + Python | Psycopg"](http://initd.org/psycopg/). _initd.org_.
37. **[^](#cite_ref-37 "Jump up")** ["SQL database drivers"](https://github.com/golang/go/wiki/SQLDrivers#drivers). _Go wiki_. golang.org. Retrieved June 22, 2015.
38. **[^](#cite_ref-38 "Jump up")** ["RPostgreSQL: R Interface to the 'PostgreSQL' Database System"](https://cran.r-project.org/web/packages/RPostgreSQL/). _CRAN_. cran.r-project.org. June 24, 2017. Retrieved August 3, 2017.
39. **[^](#cite_ref-39 "Jump up")** ["Server Programming"](https://www.postgresql.org/docs/current/server-programming.html). _Postgresql documentation_. Retrieved May 19, 2019.
40. **[^](#cite_ref-40 "Jump up")** ["DO"](https://www.postgresql.org/docs/current/sql-do.html). _Postgresql documentation_. Retrieved May 19, 2019.
41. **[^](#cite_ref-41 "Jump up")** ["PL/Python - Python Procedural Language"](https://www.postgresql.org/docs/current/plpython.html). _Postgresql documentation_. Retrieved May 19, 2019.
42. **[^](#cite_ref-42 "Jump up")** ["Procedural Languages"](https://www.postgresql.org/docs/current/static/external-pl.html). postgresql.org. March 31, 2016. Retrieved April 7, 2016.
43. **[^](#cite_ref-materialized_views_43-0 "Jump up")** ["Add a materialized view relations"](https://www.postgresql.org/message-id/E1UCJDN-00042x-0w@gemulon.postgresql.org). March 4, 2013. Retrieved March 4, 2013.
44. **[^](#cite_ref-updatable_views_44-0 "Jump up")** ["Support automatically-updatable views"](http://archives.postgresql.org/pgsql-committers/2012-12/msg00154.php). December 8, 2012. Retrieved December 8, 2012.
45. **[^](#cite_ref-recursive_views_45-0 "Jump up")** ["Add CREATE RECURSIVE VIEW syntax"](https://www.postgresql.org/message-id/E1U17NB-0006c6-DX@gemulon.postgresql.org). February 1, 2013. Retrieved February 28, 2013.
46. **[^](#cite_ref-Introduction_and_Concepts_46-0 "Jump up")** Momjian, Bruce (2001). ["Subqueries"](https://web.archive.org/web/20100809013228/http://www.postgresql.org/files/documentation/books/aw_pgsql/15467.html). [_PostgreSQL: Introduction and Concepts_](https://www.postgresql.org/files/documentation/books/aw_pgsql/15467.html). Addison-Wesley. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [0-201-70331-9](https://en.wikipedia.org/wiki/Special:BookSources/0-201-70331-9 "Special:BookSources/0-201-70331-9"). Archived from [the original](https://www.postgresql.org/files/documentation/books/aw_pgsql/node81.html) on August 9, 2010. Retrieved September 25, 2010.
47. **[^](#cite_ref-Bernier_47-0 "Jump up")** Bernier, Robert (February 2, 2006). ["Using Regular Expressions in PostgreSQL"](http://www.oreillynet.com/pub/a/databases/2006/02/02/postgresq_regexes.html). [O'Reilly Media](https://en.wikipedia.org/wiki/O%27Reilly_Media "O'Reilly Media"). Retrieved September 25, 2010.
48. **[^](#cite_ref-POODLE_48-0 "Jump up")** ["A few short notes about PostgreSQL and POODLE"](http://blog.hagander.net/archives/222-A-few-short-notes-about-PostgreSQL-and-POODLE.html). _hagander.net_.
49. **[^](#cite_ref-49 "Jump up")** Berkus, Josh (June 2, 2016). ["PostgreSQL 9.6 Beta and PGCon 2016"](https://lwn.net/Articles/689387/). _LWN.net_.
50. **[^](#cite_ref-50 "Jump up")** ["FAQ – PostgreSQL wiki"](https://wiki.postgresql.org/wiki/FAQ#How_does_PostgreSQL_use_CPU_resources.3F). _wiki.postgresql.org_. Retrieved April 13, 2017.
51. **[^](#cite_ref-51 "Jump up")** ["SEPostgreSQL Documentation – PostgreSQL wiki"](https://wiki.postgresql.org/wiki/SEPostgreSQL_Documentation). _wiki.postgresql.org_.
52. **[^](#cite_ref-52 "Jump up")** ["NB SQL 9.3 - SELinux Wiki"](https://selinuxproject.org/page/NB_SQL_9.3). _selinuxproject.org_.
53. **[^](#cite_ref-identifiers_53-0 "Jump up")** ["Case sensitivity of identifiers"](https://www.postgresql.org/docs/current/static/sql-syntax-lexical.html#SQL-SYNTAX-IDENTIFIERS). PostgreSQL Global Development Group.
54. ^ [Jump up to:_**a**_](#cite_ref-Berkus_54-0) [_**b**_](#cite_ref-Berkus_54-1) Berkus, Josh (July 6, 2007). ["PostgreSQL publishes first real benchmark"](https://web.archive.org/web/20070712092901/http://blogs.ittoolbox.com/database/soup/archives/postgresql-publishes-first-real-benchmark-17470). Archived from [the original](http://blogs.ittoolbox.com/database/soup/archives/postgresql-publishes-first-real-benchmark-17470) on July 12, 2007. Retrieved July 10, 2007.
55. **[^](#cite_ref-Vilmos_55-0 "Jump up")** Vilmos, György (September 29, 2009). ["PostgreSQL history"](http://suckit.blog.hu/2009/09/29/postgresql_history). Retrieved August 28, 2010.
56. **[^](#cite_ref-SPECJ_56-0 "Jump up")** ["SPECjAppServer2004 Result"](http://www.spec.org/jAppServer2004/results/res2007q3/jAppServer2004-20070606-00065.html). [SPEC](https://en.wikipedia.org/wiki/SPEC "SPEC"). July 6, 2007. Retrieved July 10, 2007.
57. **[^](#cite_ref-SPECjAppServer2004_57-0 "Jump up")** ["SPECjAppServer2004 Result"](http://www.spec.org/jAppServer2004/results/res2007q3/jAppServer2004-20070703-00073.html). [SPEC](https://en.wikipedia.org/wiki/SPEC "SPEC"). July 4, 2007. Retrieved September 1, 2007.
58. **[^](#cite_ref-Kernel_Resources_58-0 "Jump up")** ["Managing Kernel Resources"](https://www.postgresql.org/docs/current/static/kernel-resources.html). _PostgreSQL Manual_. PostgreSQL.org. Retrieved November 12, 2011.
59. **[^](#cite_ref-pg9hiperf_59-0 "Jump up")** Greg Smith (October 15, 2010). [_PostgreSQL 9.0 High Performance_](http://www.packtpub.com/postgresql-90-high-performance/book). [Packt Publishing](https://en.wikipedia.org/wiki/Packt_Publishing "Packt Publishing"). [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-84951-030-1](https://en.wikipedia.org/wiki/Special:BookSources/978-1-84951-030-1 "Special:BookSources/978-1-84951-030-1").
60. **[^](#cite_ref-Haas_60-0 "Jump up")** Robert Haas (April 3, 2012). ["Did I Say 32 Cores? How about 64?"](http://rhaas.blogspot.com/2012/04/did-i-say-32-cores-how-about-64.html). Retrieved April 8, 2012.
61. **[^](#cite_ref-61 "Jump up")** Khushi, Matloob (June 2015). "Benchmarking database performance for genomic data". _J Cell Biochem_. **116** (6): 877–83. [doi](https://en.wikipedia.org/wiki/Digital_object_identifier "Digital object identifier"):[10.1002/jcb.25049](https://doi.org/10.1002%2Fjcb.25049). [PMID](https://en.wikipedia.org/wiki/PubMed_Identifier "PubMed Identifier") [25560631](https://www.ncbi.nlm.nih.gov/pubmed/25560631).
62. **[^](#cite_ref-62 "Jump up")** ["PostgreSQL: Windows installers"](https://www.postgresql.org/download/windows/). _www.postgresql.org_. Retrieved October 14, 2019.
63. **[^](#cite_ref-openbsd_63-0 "Jump up")** ["postgresql-client-10.5p1 – PostgreSQL RDBMS (client)"](http://ports.su/databases/postgresql,-main). _[OpenBSD ports](https://en.wikipedia.org/wiki/OpenBSD_ports "OpenBSD ports")_. October 4, 2018. Retrieved October 10, 2018.
64. **[^](#cite_ref-OpenIndiana_64-0 "Jump up")** ["oi_151a Release Notes"](http://wiki.openindiana.org/oi/oi_151a+Release+Notes). OpenIndiana. Retrieved April 7, 2012.
65. **[^](#cite_ref-AArch64_65-0 "Jump up")** ["AArch64 planning BoF at DebConf"](http://lists.debian.org/debian-devel/2012/07/msg00536.html). _debian.org_.
66. **[^](#cite_ref-raspi_66-0 "Jump up")** Souza, Rubens (June 17, 2015). ["Step 5 (update): Installing PostgreSQL on my Raspberry Pi 1 and 2"](http://raspberrypg.org/2015/06/step-5-update-installing-postgresql-on-my-raspberry-pi-1-and-2/). _Raspberry PG_. Retrieved August 27, 2016.
67. **[^](#cite_ref-SupportedPlatforms_67-0 "Jump up")** ["Supported Platforms"](https://www.postgresql.org/docs/current/static/supported-platforms.html). PostgreSQL Global Development Group. Retrieved April 6, 2012.
68. **[^](#cite_ref-pgAdmin_68-0 "Jump up")** ["pgAdmin: PostgreSQL administration and management tools"](http://www.pgadmin.org/). _website_. Retrieved November 12, 2011.
69. **[^](#cite_ref-69 "Jump up")** ["Debian -- Details of package pgadmin3 in jessie"](https://packages.debian.org/jessie/pgadmin3). Retrieved March 10, 2017.
70. **[^](#cite_ref-70 "Jump up")** ["pgAdmin Development Team"](http://www.pgadmin.org/development/team.php). _pgadmin.org_. Retrieved June 22, 2015.
71. **[^](#cite_ref-71 "Jump up")** Dave, Page (December 7, 2014). ["The story of pgAdmin"](http://pgsnake.blogspot.co.uk/2014/12/the-story-of-pgadmin.html). _Dave's Postgres Blog_. pgsnake.blogspot.co.uk. Retrieved December 7, 2014.
72. **[^](#cite_ref-72 "Jump up")** ["pgAdmin 4 README"](https://github.com/postgres/pgadmin4/blob/master/README). Retrieved August 15, 2018.
73. **[^](#cite_ref-PHPADMIN_73-0 "Jump up")** phpPgAdmin Project (April 25, 2008). ["About phpPgAdmin"](http://phppgadmin.sourceforge.net/?page=about). Retrieved April 25, 2008.
74. **[^](#cite_ref-POSTGRESQLSTUDIO_74-0 "Jump up")** PostgreSQL Studio (October 9, 2013). ["About PostgreSQL Studio"](https://web.archive.org/web/20131007084849/http://www.postgresqlstudio.org/about/). Archived from [the original](http://www.postgresqlstudio.org/about/) on October 7, 2013. Retrieved October 9, 2013.
75. **[^](#cite_ref-TEAMPOSTGRESQL_75-0 "Jump up")** ["TeamPostgreSQL website"](http://www.teampostgresql.com/). October 3, 2013. Retrieved October 3, 2013.
76. **[^](#cite_ref-ooAsFrntEnd_76-0 "Jump up")** oooforum.org (January 10, 2010). ["Back Ends for OpenOffice"](https://web.archive.org/web/20110928093709/http://www.oooforum.org/forum/viewtopic.phtml?p=356180). Archived from [the original](http://www.oooforum.org/forum/viewtopic.phtml?p=356180) on September 28, 2011. Retrieved January 5, 2011.
77. **[^](#cite_ref-loAsFrntEnd_77-0 "Jump up")** libreoffice.org (October 14, 2012). ["Base features"](https://web.archive.org/web/20120107063659/http://www.libreoffice.org/features/base/). Archived from [the original](http://www.libreoffice.org/features/base/) on January 7, 2012. Retrieved October 14, 2012.
78. **[^](#cite_ref-tuningPGinstance_78-0 "Jump up")** Greg Smith; Robert Treat & Christopher Browne. ["Tuning your PostgreSQL server"](https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server). _Wiki_. PostgreSQL.org. Retrieved November 12, 2011.
79. **[^](#cite_ref-79 "Jump up")** ["pgDevOps"](https://web.archive.org/web/20170401220832/http://www1.bigsql.org/pgdevops/#). _BigSQL.org_. Archived from [the original](https://www.bigsql.org/pgdevops/) on April 1, 2017. Retrieved May 4, 2017.
80. **[^](#cite_ref-Cecchet_80-0 "Jump up")** Emmanuel Cecchet (May 21, 2009). [_Building PetaByte Warehouses with Unmodified PostgreSQL_](http://www.pgcon.org/2009/schedule/attachments/135_PGCon%202009%20-%20Aster%20v6.pdf) (PDF). PGCon 2009. Retrieved November 12, 2011.
81. **[^](#cite_ref-Aster_Data_81-0 "Jump up")** ["MySpace.com scales analytics for all their friends"](http://www.asterdata.com/resources/assets/cs_Aster_Data_4.0_MySpace.pdf) (PDF). case study. Aster Data. June 15, 2010. [Archived](https://web.archive.org/web/20101114141918/http://asterdata.com/resources/assets/cs_Aster_Data_4.0_MySpace.pdf) (PDF) from the original on November 14, 2010. Retrieved November 12, 2011.
82. **[^](#cite_ref-Geni_82-0 "Jump up")** ["Last Weekend's Outage"](http://www.geni.com/blog/last-weekends-outage-368211.html). _Blog_. Geni. August 1, 2011.
83. **[^](#cite_ref-OpenStreetMap_83-0 "Jump up")** ["Database"](https://wiki.openstreetmap.org/wiki/Database). _Wiki_. OpenStreetMap.
84. **[^](#cite_ref-Afilias_84-0 "Jump up")** [_PostgreSQL affiliates .ORG domain_](http://www.computerworld.com.au/index.php?id=760310963), Australia: Computer World
85. ^ [Jump up to:_**a**_](#cite_ref-begPHPpg-book_85-0) [_**b**_](#cite_ref-begPHPpg-book_85-1) [_**c**_](#cite_ref-begPHPpg-book_85-2) W. Jason Gilmore; R.H. Treat (2006). [_Beginning PHP and PostgreSQL 8: From Novice to Professional_](https://books.google.com/books?id=BiRC4JtQzFIC&pg=PA577). Apress. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-43020-136-6](https://en.wikipedia.org/wiki/Special:BookSources/978-1-43020-136-6 "Special:BookSources/978-1-43020-136-6"). Retrieved August 30, 2017.
86. **[^](#cite_ref-Sony_Online_86-0 "Jump up")** [_Sony Online opts for open-source database over Oracle_](http://www.computerworld.com/databasetopics/data/software/story/0,10801,109722,00.html), Computer World
87. **[^](#cite_ref-BASF_87-0 "Jump up")** ["A Web Commerce Group Case Study on PostgreSQL"](https://www.postgresql.org/files/about/casestudies/wcgcasestudyonpostgresqlv1.2.pdf) (PDF) (1.2 ed.). PostgreSQL.
88. **[^](#cite_ref-Reddit_88-0 "Jump up")** ["Architecture Overview"](https://github.com/reddit/reddit/wiki/Architecture-Overview#reddit-the-software). _Reddit software wiki_. Reddit. March 27, 2014. Retrieved November 25, 2014.
89. **[^](#cite_ref-89 "Jump up")** Pihlak, Martin. ["PostgreSQL @Skype"](https://wiki.postgresql.org/images/a/a9/Postgresql-at-skype.pdf) (PDF). _wiki.postgresql.org_. Retrieved January 16, 2019.
90. **[^](#cite_ref-xVM_90-0 "Jump up")** ["How Much Are You Paying For Your Database?"](https://web.archive.org/web/20090307032257/http://blogs.sun.com/marchamilton/entry/how_much_are_you_paying). Sun Microsystems blog. 2007. Archived from [the original](http://blogs.sun.com/marchamilton/entry/how_much_are_you_paying) on March 7, 2009. Retrieved December 14, 2007.
91. **[^](#cite_ref-MusicBrainz_91-0 "Jump up")** ["Database – MusicBrainz"](http://musicbrainz.org/doc/Database). MusicBrainz Wiki. Retrieved February 5, 2011.
92. **[^](#cite_ref-ISS_92-0 "Jump up")** Duncavage, Daniel P (July 13, 2010). ["NASA needs Postgres-Nagios help"](http://archives.postgresql.org/pgsql-general/2010-07/msg00394.php).
93. **[^](#cite_ref-MyYearbook_93-0 "Jump up")** Roy, Gavin M (2010). ["PostgreSQL at myYearbook.com"](https://web.archive.org/web/20110727183016/https://www.postgresqlconference.org/2010/east/talks/postgresql_at_myyearbook.com) (talk). USA East: PostgreSQL Conference. Archived from [the original](https://www.postgresqlconference.org/2010/east/talks/postgresql_at_myyearbook.com) on July 27, 2011.
94. **[^](#cite_ref-Instagram_94-0 "Jump up")** ["Keeping Instagram up with over a million new users in twelve hours"](http://instagram-engineering.tumblr.com/post/20541814340/keeping-instagram-up-with-over-a-million-new-users-in#replicationread-slaves). Instagram-engineering.tumblr.com. May 17, 2011. Retrieved July 7, 2012.
95. **[^](#cite_ref-Disqus_95-0 "Jump up")** ["Postgres at Disqus"](https://speakerdeck.com/mikeclarke/pgcon-2013-keynote-postgres-at-disqus). Retrieved May 24, 2013.
96. **[^](#cite_ref-TripAdvisor_96-0 "Jump up")** Kelly, Matthew (March 27, 2015). [_At the Heart of a Giant: Postgres at TripAdvisor_](https://web.archive.org/web/20150723181100/http://www.pgconf.us/2015/event/95/). PGConf US 2015. Archived from [the original](http://www.pgconf.us/2015/event/95/) on July 23, 2015. Retrieved July 23, 2015.([Presentation video](https://www.youtube.com/watch?v=YquXmwZNnfg))
97. **[^](#cite_ref-97 "Jump up")** ["Yandex.Mail's successful migration from Oracle to Postgres \[pdf\]"](https://news.ycombinator.com/item?id=12489055). _Hacker News: news.ycombinator.com_. Retrieved September 28, 2016.
98. ^ [Jump up to:_**a**_](#cite_ref-pg9AdminCookEdt2-book_98-0) [_**b**_](#cite_ref-pg9AdminCookEdt2-book_98-1) S. Riggs; G. Ciolli; H. Krosing; G. Bartolini (2015). [_PostgreSQL 9 Administration Cookbook - Second Edition_](https://books.google.com/books?id=rYrwCAAAQBAJ&pg=PA3). Packt. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-84951-906-9](https://en.wikipedia.org/wiki/Special:BookSources/978-1-84951-906-9 "Special:BookSources/978-1-84951-906-9"). Retrieved September 5, 2017.
99. **[^](#cite_ref-99 "Jump up")** ["Met Office swaps Oracle for PostgreSQL"](http://www.computerweekly.com/ezine/Computer-Weekly/The-Met-Office-turns-to-open-source/Met-Office-swaps-Oracle-for-PostgreSQL). _computerweekly.com_. Retrieved September 5, 2017.
100.    **[^](#cite_ref-100 "Jump up")** [Oracle Database](https://en.wikipedia.org/wiki/Oracle_Database "Oracle Database")
101.    **[^](#cite_ref-101 "Jump up")** ["Open Source Software"](https://flightaware.com/about/code/). _FlightAware_. Retrieved November 22, 2017.
102.    **[^](#cite_ref-102 "Jump up")** ["Ansible at Grofers (Part 2) — Managing PostgreSQL – Lambda - The Grofers Engineering Blog"](https://lambda.grofers.com/ansible-at-grofers-part-2-managing-postgresql-c4069ce5855b). _Lambda - The Grofers Engineering Blog_. February 28, 2017. Retrieved September 5, 2018.
103.    **[^](#cite_ref-103 "Jump up")** McMahon, Philip; Chiorean, Maria-Livia; Coleman, Susie; Askoolum, Akash (November 30, 2018). ["Digital Blog: Bye bye Mongo, Hello Postgres"](https://www.theguardian.com/info/2018/nov/30/bye-bye-mongo-hello-postgres). _[The Guardian](https://en.wikipedia.org/wiki/The_Guardian "The Guardian")_. [ISSN](https://en.wikipedia.org/wiki/International_Standard_Serial_Number "International Standard Serial Number") [0261-3077](https://www.worldcat.org/issn/0261-3077).
104.    **[^](#cite_ref-Heroku_104-0 "Jump up")** Alex Williams (April 1, 2013). ["Heroku Forces Customer Upgrade To Fix Critical PostgreSQL Security Hole"](https://techcrunch.com/2013/04/01/heroku-forces-customer-upgrade-to-fix-critical-postgresql-security-hole/). _TechCrunch_.
105.    **[^](#cite_ref-Darrow_105-0 "Jump up")** Barb Darrow (November 11, 2013). ["Heroku gussies up Postgres with database roll-back and proactive alerts"](http://gigaom.com/2013/11/11/heroku-gussies-up-postgres-with-database-roll-back-and-proactive-alerts/). GigaOM.
106.    **[^](#cite_ref-Kerstiens_106-0 "Jump up")** Craig Kerstiens (September 26, 2013). ["WAL-E and Continuous Protection with Heroku Postgres"](https://blog.heroku.com/archives/2013/9/26/wal_e_and_continuous_protection_with_heroku_postgres). Heroku blog.
107.    **[^](#cite_ref-Techweekeurope_107-0 "Jump up")** ["EnterpriseDB Offers Up Postgres Plus Cloud Database"](http://www.techweekeurope.co.uk/news/enterprisedb-offers-up-postgres-plus-cloud-database-57030). Techweekeurope.co.uk. January 27, 2012. Retrieved July 7, 2012.
108.    **[^](#cite_ref-108 "Jump up")** O'Doherty, Paul; Asselin, Stephane (2014). "3: VMware Workspace Architecture". [_VMware Horizon Suite: Building End-User Services_](https://books.google.com/books?id=1mTYAwAAQBAJ). VMware Press Technology. Upper Saddle River, NJ: VMware Press. p. 65. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-0-13-347910-2](https://en.wikipedia.org/wiki/Special:BookSources/978-0-13-347910-2 "Special:BookSources/978-0-13-347910-2"). Retrieved September 19, 2016. "In addition to the open source version of PostgreSQL, VMware offers vFabric Postgres, or vPostgres. vPostgres is a PostgreSQL virtual appliance that has been tuned for virtual environments."
109.    **[^](#cite_ref-Sargent_109-0 "Jump up")** Al Sargent (May 15, 2012). ["Introducing VMware vFabric Suite 5.1: Automated Deployment, New Components, and Open Source Support"](https://blogs.vmware.com/vfabric/2012/05/announcing-vmware-vfabric-suite-51.html). VMware blogs.
110.    **[^](#cite_ref-aws.typepad.com_110-0 "Jump up")** Jeff (November 14, 2013). ["Amazon RDS for PostgreSQL – Now Available"](http://aws.typepad.com/aws/2013/11/amazon-rds-for-postgresql-now-available.html). Amazon Web Services Blog.
111.    **[^](#cite_ref-Williams_111-0 "Jump up")** Alex Williams (November 14, 2013). ["PostgreSQL Now Available On Amazon's Relational Database Service"](https://techcrunch.com/2013/11/14/postgressql-now-available-on-amazons-relational-database-service/). _TechCrunch_.
112.    **[^](#cite_ref-112 "Jump up")** ["Amazon Aurora Update – PostgreSQL Compatibility"](https://aws.amazon.com/blogs/aws/amazon-aurora-update-postgresql-compatibility/). _AWS Blog_. November 30, 2016. Retrieved December 1, 2016.
113.    **[^](#cite_ref-113 "Jump up")** ["Announcing Azure Database for PostgreSQL"](https://azure.microsoft.com/en-us/blog/azure-database-for-mysql-public-preview/). _Azure Blog_. Retrieved June 19, 2019.
114.    **[^](#cite_ref-114 "Jump up")** ["Fully Managed PostgreSQL Hosting on AWS and Azure Launches in Time For Legacy Migrations"](https://scalegrid.io/blog/fully-managed-postgresql-hosting-on-aws-and-azure-launches-in-time-for-legacy-migrations/). _ScaleGrid Blog_. Retrieved August 16, 2019.
115.    **[^](#cite_ref-115 "Jump up")** ["Versioning policy"](https://www.postgresql.org/support/versioning/). PostgreSQL Global Development Group. Retrieved October 4, 2018.
116.    **[^](#cite_ref-116 "Jump up")** Vaas, Lisa (December 2, 2002). ["Databases Target Enterprises"](http://www.eweek.com/c/a/Database/Databases-Target-Enterprises). _[eWeek](https://en.wikipedia.org/wiki/EWeek "EWeek")_. Retrieved October 29, 2016.
117.    **[^](#cite_ref-117 "Jump up")** Krill, Paul (November 20, 2003). ["PostgreSQL boosts open source database"](http://www.infoworld.com/article/2670451/database/postgresql-boosts-open-source-database.html). _[InfoWorld](https://en.wikipedia.org/wiki/InfoWorld "InfoWorld")_. Retrieved October 21, 2016.
118.    **[^](#cite_ref-118 "Jump up")** Krill, Paul (January 19, 2005). ["PostgreSQL open source database boasts Windows boost"](http://www.infoworld.com/article/2668622/operating-systems/postgresql-open-source-database-boasts-windows-boost.html). _[InfoWorld](https://en.wikipedia.org/wiki/InfoWorld "InfoWorld")_. Retrieved November 2, 2016.
119.    **[^](#cite_ref-119 "Jump up")** Weiss, Todd R. (December 5, 2006). ["Version 8.2 of open-source PostgreSQL DB released"](http://www.computerworld.com/article/2548483). _[Computerworld](https://en.wikipedia.org/wiki/Computerworld "Computerworld")_. Retrieved October 17, 2016.
120.    **[^](#cite_ref-120 "Jump up")** Gilbertson, Scott (February 5, 2008). ["PostgreSQL 8.3: Open Source Database Promises Blazing Speed"](https://www.wired.com/2008/02/postgresql_8dot3_open_source_database_promises_blazing_speed/). _[Wired](https://en.wikipedia.org/wiki/Wired_(magazine) "Wired (magazine)")_. Retrieved October 17, 2016.
121.    **[^](#cite_ref-121 "Jump up")** Huber, Mathias (July 2, 2009). ["PostgreSQL 8.4 Proves Feature-Rich"](http://www.linux-magazine.com/Online/News/PostgreSQL-8.4-Proves-Feature-Rich/(language)/eng-US). _[Linux Magazine](https://en.wikipedia.org/wiki/Linux_Magazine "Linux Magazine")_. Retrieved October 17, 2016.
122.    **[^](#cite_ref-122 "Jump up")** Brockmeier, Joe (September 30, 2010). ["Five Enterprise Features in PostgreSQL 9"](https://www.linux.com/news/five-enterprise-features-postgresql-9). _[Linux.com](https://en.wikipedia.org/wiki/Linux.com "Linux.com")_. [Linux Foundation](https://en.wikipedia.org/wiki/Linux_Foundation "Linux Foundation"). Retrieved February 6, 2017.
123.    **[^](#cite_ref-123 "Jump up")** Timothy Prickett Morgan (September 12, 2011). ["PostgreSQL revs to 9.1, aims for enterprise"](https://www.theregister.co.uk/2011/09/12/postgresql_9_1_cloud_server/). _[The Register](https://en.wikipedia.org/wiki/The_Register "The Register")_. Retrieved February 6, 2017.
124.    **[^](#cite_ref-124 "Jump up")** ["PostgreSQL: PostgreSQL 9.2 released"](https://www.postgresql.org/about/news/1415/). _www.postgresql.org_.
125.    **[^](#cite_ref-125 "Jump up")** ["Reintroducing Hstore for PostgreSQL"](https://www.infoq.com/news/2013/11/Nested-Hstore). _InfoQ_.
126.    **[^](#cite_ref-126 "Jump up")** Richard, Chirgwin (January 7, 2016). ["Say oops, UPSERT your head: PostgreSQL version 9.5 has landed"](https://www.theregister.co.uk/2016/01/07/postgresql_95_lands/). _[The Register](https://en.wikipedia.org/wiki/The_Register "The Register")_. Retrieved October 17, 2016.
127.    **[^](#cite_ref-127 "Jump up")** ["PostgreSQL: Documentation: 10: Chapter 31. Logical Replication"](https://www.postgresql.org/docs/10/logical-replication.html). _www.postgresql.org_.
128.    **[^](#cite_ref-128 "Jump up")** ["PostgreSQL 11 Released"](https://www.postgresql.org/about/news/1894/). Retrieved October 18, 2018.
129.    **[^](#cite_ref-129 "Jump up")** ["PostgreSQLRelease Notes"](https://www.postgresql.org/docs/11/static/release-11.html). Retrieved October 18, 2018.
130.    **[^](#cite_ref-130 "Jump up")** ["PostgreSQL: PostgreSQL 12 Released!"](https://www.postgresql.org/about/news/1976/). _www.postgresql.org_.

## Further reading\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=31 "Edit section: Further reading")]

-   Obe, Regina; Hsu, Leo (July 8, 2012). [_PostgreSQL: Up and Running_](http://www.postgresonline.com/store.php?asin=1449326331). [O'Reilly](https://en.wikipedia.org/wiki/O%27Reilly_Media "O'Reilly Media"). [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-4493-2633-3](https://en.wikipedia.org/wiki/Special:BookSources/978-1-4493-2633-3 "Special:BookSources/978-1-4493-2633-3").
-   Krosing, Hannu; Roybal, Kirk (June 15, 2013). [_PostgreSQL Server Programming_](http://www.2ndquadrant.com/books/) (second ed.). [Packt Publishing](https://en.wikipedia.org/wiki/Packt_Publishing "Packt Publishing"). [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-84951-698-3](https://en.wikipedia.org/wiki/Special:BookSources/978-1-84951-698-3 "Special:BookSources/978-1-84951-698-3").
-   Riggs, Simon; Krosing, Hannu (October 27, 2010). [_PostgreSQL 9 Administration Cookbook_](http://www.2ndquadrant.com/books/) (second ed.). [Packt Publishing](https://en.wikipedia.org/wiki/Packt_Publishing "Packt Publishing"). [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-84951-028-8](https://en.wikipedia.org/wiki/Special:BookSources/978-1-84951-028-8 "Special:BookSources/978-1-84951-028-8").
-   Smith, Greg (October 15, 2010). [_PostgreSQL 9 High Performance_](http://www.2ndquadrant.com/books/). [Packt Publishing](https://en.wikipedia.org/wiki/Packt_Publishing "Packt Publishing"). [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [978-1-84951-030-1](https://en.wikipedia.org/wiki/Special:BookSources/978-1-84951-030-1 "Special:BookSources/978-1-84951-030-1").
-   Gilmore, W. Jason; Treat, Robert (February 27, 2006). [_Beginning PHP and PostgreSQL 8: From Novice to Professional_](https://web.archive.org/web/20090708113944/http://www.apress.com/book/view/1590595475). [Apress](https://en.wikipedia.org/wiki/Apress "Apress"). p. 896. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [1-59059-547-5](https://en.wikipedia.org/wiki/Special:BookSources/1-59059-547-5 "Special:BookSources/1-59059-547-5"). Archived from [the original](http://www.apress.com/book/view/1590595475) on July 8, 2009. Retrieved April 28, 2009.
-   Douglas, Korry (August 5, 2005). [_PostgreSQL_](http://www.informit.com/store/product.aspx?isbn=0672327562) (second ed.). [Sams](https://en.wikipedia.org/wiki/Sams_Publishing "Sams Publishing"). p. 1032. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [0-672-32756-2](https://en.wikipedia.org/wiki/Special:BookSources/0-672-32756-2 "Special:BookSources/0-672-32756-2").
-   Matthew, Neil; Stones, Richard (April 6, 2005). [_Beginning Databases with PostgreSQL_](https://web.archive.org/web/20090409150911/http://www.apress.com/book/view/9781590594780) (second ed.). [Apress](https://en.wikipedia.org/wiki/Apress "Apress"). p. 664. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [1-59059-478-9](https://en.wikipedia.org/wiki/Special:BookSources/1-59059-478-9 "Special:BookSources/1-59059-478-9"). Archived from [the original](http://www.apress.com/book/view/9781590594780) on April 9, 2009. Retrieved April 28, 2009.
-   Worsley, John C; Drake, Joshua D (January 2002). [_Practical PostgreSQL_](https://archive.org/details/practicalpostgre00wors). [O'Reilly Media](https://en.wikipedia.org/wiki/O%27Reilly_Media "O'Reilly Media"). p. 636. [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number "International Standard Book Number") [1-56592-846-6](https://en.wikipedia.org/wiki/Special:BookSources/1-56592-846-6 "Special:BookSources/1-56592-846-6").

## External links\[[edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit&section=32 "Edit section: External links")]

| ![](PostgreSQL%20-%20Wikipedia_files/30px-Commons-logo.png) | Wikimedia Commons has media related to _**[PostgreSQL](https://commons.wikimedia.org/wiki/Category:PostgreSQL "commons:Category:PostgreSQL")**_. |
| ----------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |

| ![](PostgreSQL%20-%20Wikipedia_files/40px-Wikibooks-logo-en-noslogan.png) | Wikibooks has a book on the topic of: _**[PostgreSQL](https://en.wikibooks.org/wiki/PostgreSQL "wikibooks:PostgreSQL")**_ |
| ------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |

-   [Official website](https://www.postgresql.org/) [![Edit this at Wikidata](PostgreSQL%20-%20Wikipedia_files/10px-Blue_pencil.png)](https://www.wikidata.org/wiki/Q192490#P856 "Edit this at Wikidata")
-   [PostgreSQL](https://curlie.org/Computers/Software/Databases/PostgreSQL) at [Curlie](https://en.wikipedia.org/wiki/Curlie "Curlie")
-   [PostgreSQL](https://github.com/postgres) on [GitHub](https://en.wikipedia.org/wiki/GitHub "GitHub")

| [show](<>)-   [v](https://en.wikipedia.org/wiki/Template:Software_in_the_Public_Interest "Template:Software in the Public Interest")
-   [t](https://en.wikipedia.org/wiki/Template_talk:Software_in_the_Public_Interest "Template talk:Software in the Public Interest")
-   [e](https://en.wikipedia.org/w/index.php?title=Template:Software_in_the_Public_Interest&action=edit)[Software in the Public Interest](https://en.wikipedia.org/wiki/Software_in_the_Public_Interest "Software in the Public Interest") |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| People                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | -   [Martin Michlmayr](https://en.wikipedia.org/wiki/Martin_Michlmayr "Martin Michlmayr") (President)
-   [Bdale Garbee](https://en.wikipedia.org/wiki/Bdale_Garbee "Bdale Garbee")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Projects                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | -   [0 A.D.](https://en.wikipedia.org/wiki/0_A.D._(video_game) "0 A.D. (video game)")
-   [Arch Linux](https://en.wikipedia.org/wiki/Arch_Linux "Arch Linux")
-   [Debian](https://en.wikipedia.org/wiki/Debian "Debian")
-   [Drizzle](https://en.wikipedia.org/wiki/Drizzle_(database_server) "Drizzle (database server)")
-   [Drupal](https://en.wikipedia.org/wiki/Drupal "Drupal")
-   [FFmpeg](https://en.wikipedia.org/wiki/FFmpeg "FFmpeg")
-   [Fluxbox](https://en.wikipedia.org/wiki/Fluxbox "Fluxbox")
-   [freedesktop.org](https://en.wikipedia.org/wiki/Freedesktop.org "Freedesktop.org")
-   [FreedomBox](https://en.wikipedia.org/wiki/FreedomBox "FreedomBox")
-   [Gallery Project](https://en.wikipedia.org/wiki/Gallery_Project "Gallery Project")
-   [GNU TeXmacs](https://en.wikipedia.org/wiki/GNU_TeXmacs "GNU TeXmacs")
-   [GNUstep](https://en.wikipedia.org/wiki/GNUstep "GNUstep")
-   [Jenkins](https://en.wikipedia.org/wiki/Jenkins_(software) "Jenkins (software)")
-   [LibreOffice](https://en.wikipedia.org/wiki/LibreOffice "LibreOffice")
-   [MinGW](https://en.wikipedia.org/wiki/MinGW "MinGW")
-   [Open and Free Technology Community](https://en.wikipedia.org/wiki/Open_and_Free_Technology_Community "Open and Free Technology Community")
-   [Open Bioinformatics Foundation](https://en.wikipedia.org/wiki/Open_Bioinformatics_Foundation "Open Bioinformatics Foundation")
-   [Open64](https://en.wikipedia.org/wiki/Open64 "Open64")
-   [OpenEmbedded](https://en.wikipedia.org/wiki/OpenEmbedded "OpenEmbedded")
-   [OpenVAS](https://en.wikipedia.org/wiki/OpenVAS "OpenVAS")
-   [OpenWrt](https://en.wikipedia.org/wiki/OpenWrt "OpenWrt")
-   [PostgreSQL](<>)
-   [Privoxy](https://en.wikipedia.org/wiki/Privoxy "Privoxy")
-   [SproutCore](https://en.wikipedia.org/wiki/SproutCore "SproutCore")
-   [X.Org Foundation](https://en.wikipedia.org/wiki/X.Org_Foundation "X.Org Foundation")
-   [YafaRay](https://en.wikipedia.org/wiki/YafaRay "YafaRay") |

<!-- 
NewPP limit report
Parsed by mw1269
Cached time: 20191112002442
Cache expiry: 3600
Dynamic content: true
Complications: [vary‐revision‐sha1]
CPU time usage: 1.848 seconds
Real time usage: 2.487 seconds
Preprocessor visited node count: 10262/1000000
Preprocessor generated node count: 0/1500000
Post‐expand include size: 253945/2097152 bytes
Template argument size: 10607/2097152 bytes
Highest expansion depth: 27/40
Expensive parser function count: 11/500
Unstrip recursion depth: 1/20
Unstrip post‐expand size: 392254/5000000 bytes
Number of Wikibase entities loaded: 5/400
Lua time usage: 1.015/10.000 seconds
Lua memory usage: 11.08 MB/50 MB
Lua Profile:
    ?                                                                240 ms       20.3%
    Scribunto_LuaSandboxCallback::getEntity                          120 ms       10.2%
    recursiveClone <mwInit.lua:41>                                   100 ms        8.5%
    Scribunto_LuaSandboxCallback::getAllExpandedArguments            100 ms        8.5%
    Scribunto_LuaSandboxCallback::getExpandedArgument                100 ms        8.5%
    Scribunto_LuaSandboxCallback::callParserFunction                  80 ms        6.8%
    chunk <Module:Citation/CS1>                                       60 ms        5.1%
    Scribunto_LuaSandboxCallback::match                               60 ms        5.1%
    type                                                              40 ms        3.4%
    (for generator)                                                   40 ms        3.4%
    [others]                                                         240 ms       20.3%
-->

<!--
Transclusion expansion time report (%,ms,calls,template)
100.00% 2280.510      1 -total
 47.29% 1078.369      1 Template:Reflist
 25.48%  581.012     95 Template:Cite_web
 25.13%  573.001      3 Template:Infobox
 23.65%  539.390      1 Template:Infobox_software
 13.48%  307.336      2 Template:Wikidata
  5.06%  115.481      1 Template:Timeline_PostgreSQL
  3.84%   87.536      7 Template:Cite_news
  3.78%   86.224      2 Template:Start_date_and_age
  3.58%   81.587     14 Template:Cite_book
-->

<!-- Saved in parser cache with key enwiki:pcache:idhash:23824-0!canonical and timestamp 20191112002440 and revision id 925101079
 -->

![](//en.wikipedia.org/wiki/Special:CentralAutoLogin/start?type=1x1)

Retrieved from "<https://en.wikipedia.org/w/index.php?title=PostgreSQL&oldid=925101079>"

[Categories](https://en.wikipedia.org/wiki/Help:Category "Help:Category"):

-   [Client-server database management systems](https://en.wikipedia.org/wiki/Category:Client-server_database_management_systems "Category:Client-server database management systems")
-   [Cross-platform software](https://en.wikipedia.org/wiki/Category:Cross-platform_software "Category:Cross-platform software")
-   [Free database management systems](https://en.wikipedia.org/wiki/Category:Free_database_management_systems "Category:Free database management systems")
-   [Free software programmed in C](https://en.wikipedia.org/wiki/Category:Free_software_programmed_in_C "Category:Free software programmed in C")
-   [ORDBMS software for Linux](https://en.wikipedia.org/wiki/Category:ORDBMS_software_for_Linux "Category:ORDBMS software for Linux")
-   [RDBMS software for Linux](https://en.wikipedia.org/wiki/Category:RDBMS_software_for_Linux "Category:RDBMS software for Linux")
-   [PostgreSQL](https://en.wikipedia.org/wiki/Category:PostgreSQL "Category:PostgreSQL")

Hidden categories:

-   [Pages using Timeline](https://en.wikipedia.org/wiki/Category:Pages_using_Timeline "Category:Pages using Timeline")
-   [CS1 Czech-language sources (cs)](https://en.wikipedia.org/wiki/Category:CS1_Czech-language_sources_(cs) "Category:CS1 Czech-language sources (cs)")
-   [Articles with short description](https://en.wikipedia.org/wiki/Category:Articles_with_short_description "Category:Articles with short description")
-   [Use mdy dates from February 2019](https://en.wikipedia.org/wiki/Category:Use_mdy_dates_from_February_2019 "Category:Use mdy dates from February 2019")
-   [All articles with unsourced statements](https://en.wikipedia.org/wiki/Category:All_articles_with_unsourced_statements "Category:All articles with unsourced statements")
-   [Articles with unsourced statements from February 2017](https://en.wikipedia.org/wiki/Category:Articles_with_unsourced_statements_from_February_2017 "Category:Articles with unsourced statements from February 2017")
-   [Articles containing potentially dated statements from 2010](https://en.wikipedia.org/wiki/Category:Articles_containing_potentially_dated_statements_from_2010 "Category:Articles containing potentially dated statements from 2010")
-   [All articles containing potentially dated statements](https://en.wikipedia.org/wiki/Category:All_articles_containing_potentially_dated_statements "Category:All articles containing potentially dated statements")
-   [All articles lacking reliable references](https://en.wikipedia.org/wiki/Category:All_articles_lacking_reliable_references "Category:All articles lacking reliable references")
-   [Articles lacking reliable references from October 2017](https://en.wikipedia.org/wiki/Category:Articles_lacking_reliable_references_from_October_2017 "Category:Articles lacking reliable references from October 2017")
-   [Commons category link from Wikidata](https://en.wikipedia.org/wiki/Category:Commons_category_link_from_Wikidata "Category:Commons category link from Wikidata")
-   [Articles with Curlie links](https://en.wikipedia.org/wiki/Category:Articles_with_Curlie_links "Category:Articles with Curlie links")

## Navigation menu

### Personal tools

-   Not logged in
-   [Talk](https://en.wikipedia.org/wiki/Special:MyTalk "Discussion about edits from this IP address \[Alt+Shift+n]")
-   [Contributions](https://en.wikipedia.org/wiki/Special:MyContributions "A list of edits made from this IP address \[Alt+Shift+y]")
-   [Create account](https://en.wikipedia.org/w/index.php?title=Special:CreateAccount&returnto=PostgreSQL "You are encouraged to create an account and log in; however, it is not mandatory")
-   [Log in](https://en.wikipedia.org/w/index.php?title=Special:UserLogin&returnto=PostgreSQL "You're encouraged to log in; however, it's not mandatory. \[Alt+Shift+o]")

### Namespaces

-   [Article](https://en.wikipedia.org/wiki/PostgreSQL "View the content page \[Alt+Shift+c]")
-   [Talk](https://en.wikipedia.org/wiki/Talk:PostgreSQL "Discussion about the content page \[Alt+Shift+t]")

\[ ]

### Variants



### Views

-   [Read](https://en.wikipedia.org/wiki/PostgreSQL)
-   [Edit](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=edit "Edit this page \[Alt+Shift+e]")
-   [View history](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=history "Past revisions of this page \[Alt+Shift+h]")

\[ ]

### More



### Search

Search WikipediaGo

[](https://en.wikipedia.org/wiki/Main_Page "Visit the main page")

### Navigation

-   [Main page](https://en.wikipedia.org/wiki/Main_Page "Visit the main page \[Alt+Shift+z]")
-   [Contents](https://en.wikipedia.org/wiki/Portal:Contents "Guides to browsing Wikipedia")
-   [Featured content](https://en.wikipedia.org/wiki/Portal:Featured_content "Featured content – the best of Wikipedia")
-   [Current events](https://en.wikipedia.org/wiki/Portal:Current_events "Find background information on current events")
-   [Random article](https://en.wikipedia.org/wiki/Special:Random "Load a random article \[Alt+Shift+x]")
-   [Donate to Wikipedia](https://donate.wikimedia.org/wiki/Special:FundraiserRedirector?utm_source=donate&utm_medium=sidebar&utm_campaign=C13_en.wikipedia.org&uselang=en "Support us")
-   [Wikipedia store](https://shop.wikimedia.org/ "Visit the Wikipedia store")

### Interaction

-   [Help](https://en.wikipedia.org/wiki/Help:Contents "Guidance on how to use and edit Wikipedia")
-   [About Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:About "Find out about Wikipedia")
-   [Community portal](https://en.wikipedia.org/wiki/Wikipedia:Community_portal "About the project, what you can do, where to find things")
-   [Recent changes](https://en.wikipedia.org/wiki/Special:RecentChanges "A list of recent changes in the wiki \[Alt+Shift+r]")
-   [Contact page](https://en.wikipedia.org/wiki/Wikipedia:Contact_us "How to contact Wikipedia")

### Tools

-   [What links here](https://en.wikipedia.org/wiki/Special:WhatLinksHere/PostgreSQL "List of all English Wikipedia pages containing links to this page \[Alt+Shift+j]")
-   [Related changes](https://en.wikipedia.org/wiki/Special:RecentChangesLinked/PostgreSQL "Recent changes in pages linked from this page \[Alt+Shift+k]")
-   [Upload file](https://en.wikipedia.org/wiki/Wikipedia:File_Upload_Wizard "Upload files \[Alt+Shift+u]")
-   [Special pages](https://en.wikipedia.org/wiki/Special:SpecialPages "A list of all special pages \[Alt+Shift+q]")
-   [Permanent link](https://en.wikipedia.org/w/index.php?title=PostgreSQL&oldid=925101079 "Permanent link to this revision of the page")
-   [Page information](https://en.wikipedia.org/w/index.php?title=PostgreSQL&action=info "More information about this page")
-   [Wikidata item](https://www.wikidata.org/wiki/Special:EntityPage/Q192490 "Link to connected data repository item \[Alt+Shift+g]")
-   [Cite this page](https://en.wikipedia.org/w/index.php?title=Special:CiteThisPage&page=PostgreSQL&id=925101079 "Information on how to cite this page")

### In other projects

-   [Wikimedia Commons](https://commons.wikimedia.org/wiki/Category:PostgreSQL)
-   [MediaWiki](https://www.mediawiki.org/wiki/Manual:PostgreSQL)
-   [Wikibooks](https://en.wikibooks.org/wiki/PostgreSQL)

### Print/export

-   [Create a book](https://en.wikipedia.org/w/index.php?title=Special:Book&bookcmd=book_creator&referer=PostgreSQL)
-   [Download as PDF](https://en.wikipedia.org/w/index.php?title=Special:ElectronPdf&page=PostgreSQL&action=show-download-screen)
-   [Printable version](https://en.wikipedia.org/w/index.php?title=PostgreSQL&printable=yes "Printable version of this page \[Alt+Shift+p]")

### Languages

-   [Afrikaans](https://af.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Afrikaans")
-   [العربية](https://ar.wikipedia.org/wiki/%D8%A8%D9%88%D8%B3%D8%AA%D8%AC%D8%B1%D9%8A_%D8%A5%D8%B3_%D9%83%D9%8A%D9%88_%D8%A5%D9%84 "بوستجري إس كيو إل – Arabic")
-   [Azərbaycanca](https://az.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Azerbaijani")
-   [বাংলা](https://bn.wikipedia.org/wiki/%E0%A6%AA%E0%A7%8B%E0%A6%B8%E0%A7%8D%E0%A6%9F%E0%A6%9C%E0%A6%BF%E0%A6%86%E0%A6%B0%E0%A6%87-%E0%A6%8F%E0%A6%B8%E0%A6%95%E0%A6%BF%E0%A6%89%E0%A6%8F%E0%A6%B2 "পোস্টজিআরই-এসকিউএল – Bangla")
-   [Български](https://bg.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Bulgarian")
-   [Català](https://ca.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Catalan")
-   [Čeština](https://cs.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Czech")
-   [Dansk](https://da.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Danish")
-   [Deutsch](https://de.wikipedia.org/wiki/PostgreSQL "PostgreSQL – German")
-   [Eesti](https://et.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Estonian")
-   [Ελληνικά](https://el.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Greek")
-   [Español](https://es.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Spanish")
-   [Esperanto](https://eo.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Esperanto")
-   [Euskara](https://eu.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Basque")
-   [فارسی](https://fa.wikipedia.org/wiki/%D9%BE%D8%B3%D8%AA%DA%AF%D8%B1%D8%B3%E2%80%8C%DA%A9%DB%8C%D9%88%D8%A7%D9%84 "پستگرس‌کیوال – Persian")
-   [Français](https://fr.wikipedia.org/wiki/PostgreSQL "PostgreSQL – French")
-   [한국어](https://ko.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Korean")
-   [Bahasa Indonesia](https://id.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Indonesian")
-   [Italiano](https://it.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Italian")
-   [עברית](https://he.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Hebrew")
-   [Қазақша](https://kk.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Kazakh")
-   [Latviešu](https://lv.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Latvian")
-   [Lietuvių](https://lt.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Lithuanian")
-   [Magyar](https://hu.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Hungarian")
-   [Malagasy](https://mg.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Malagasy")
-   [മലയാളം](https://ml.wikipedia.org/wiki/%E0%B4%AA%E0%B5%8B%E0%B4%B8%E0%B5%8D%E0%B4%B1%E0%B5%8D%E0%B4%B1%E0%B5%8D%E0%B4%97%E0%B5%8D%E0%B4%B0%E0%B5%87%E0%B4%8E%E0%B4%B8%E0%B5%8D%E0%B4%95%E0%B5%8D%E0%B4%AF%E0%B5%81%E0%B4%8E%E0%B5%BD "പോസ്റ്റ്ഗ്രേഎസ്ക്യുഎൽ – Malayalam")
-   [Bahasa Melayu](https://ms.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Malay")
-   [Nederlands](https://nl.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Dutch")
-   [日本語](https://ja.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Japanese")
-   [Norsk](https://no.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Norwegian")
-   [Polski](https://pl.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Polish")
-   [Português](https://pt.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Portuguese")
-   [Română](https://ro.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Romanian")
-   [Русский](https://ru.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Russian")
-   [Scots](https://sco.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Scots")
-   [Slovenčina](https://sk.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Slovak")
-   [Српски / srpski](https://sr.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Serbian")
-   [Srpskohrvatski / српскохрватски](https://sh.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Serbo-Croatian")
-   [Suomi](https://fi.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Finnish")
-   [Svenska](https://sv.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Swedish")
-   [தமிழ்](https://ta.wikipedia.org/wiki/%E0%AE%AA%E0%AF%8B%E0%AE%B8%E0%AF%8D%E0%AE%95%E0%AE%BF%E0%AE%B0%E0%AF%86%E0%AE%B8%E0%AF%8D%E0%AE%95%E0%AF%81%E0%AE%AF%E0%AF%86%E0%AE%B2%E0%AF%8D "போஸ்கிரெஸ்குயெல் – Tamil")
-   [ไทย](https://th.wikipedia.org/wiki/%E0%B9%82%E0%B8%9E%E0%B8%AA%E0%B8%95%E0%B9%8C%E0%B9%80%E0%B8%81%E0%B8%A3%E0%B8%AA%E0%B8%84%E0%B8%B4%E0%B8%A7%E0%B9%80%E0%B8%AD%E0%B8%A5 "โพสต์เกรสคิวเอล – Thai")
-   [Türkçe](https://tr.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Turkish")
-   [Українська](https://uk.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Ukrainian")
-   [Tiếng Việt](https://vi.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Vietnamese")
-   [中文](https://zh.wikipedia.org/wiki/PostgreSQL "PostgreSQL – Chinese")
-   37 more

[Edit links](https://www.wikidata.org/wiki/Special:EntityPage/Q192490#sitelinks-wikipedia "Edit interlanguage links")

-   This page was last edited on 7 November 2019, at 21:18(UTC).
-   Text is available under the [Creative Commons Attribution-ShareAlike License](https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License)[](https://creativecommons.org/licenses/by-sa/3.0/); additional terms may apply. By using this site, you agree to the [Terms of Use](https://foundation.wikimedia.org/wiki/Terms_of_Use) and [Privacy Policy](https://foundation.wikimedia.org/wiki/Privacy_policy). Wikipedia® is a registered trademark of the [Wikimedia Foundation, Inc.](https://www.wikimediafoundation.org/), a non-profit organization.


-   [Privacy policy](https://foundation.wikimedia.org/wiki/Privacy_policy "wmf:Privacy policy")
-   [About Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:About "Wikipedia:About")
-   [Disclaimers](https://en.wikipedia.org/wiki/Wikipedia:General_disclaimer "Wikipedia:General disclaimer")
-   [Contact Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Contact_us)
-   [Developers](https://www.mediawiki.org/wiki/Special:MyLanguage/How_to_contribute)
-   [Statistics](https://stats.wikimedia.org/v2/#/en.wikipedia.org)
-   [Cookie statement](https://foundation.wikimedia.org/wiki/Cookie_statement)
-   [Mobile view](https://en.m.wikipedia.org/w/index.php?title=PostgreSQL&mobileaction=toggle_view_mobile)
-   [Enable previews](#)


-   [![Wikimedia Foundation](PostgreSQL%20-%20Wikipedia_files/wikimedia-button.png)](https://wikimediafoundation.org/)
-   [![Powered by MediaWiki](PostgreSQL%20-%20Wikipedia_files/poweredby_mediawiki_88x31.png)](https://www.mediawiki.org/)

[](https://en.wikipedia.org/wiki/PostgreSQL?action=edit)
